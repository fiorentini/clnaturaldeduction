\section{Translation from $\NCR$ into $\NC$}

We inductively define the translation $\phi$ from $\NCR$ into $\NC$.
Given a set $\Lambda$ of formulas $\neg \Lambda$ denotes the set
$\{\neg A~|~A\in\Lambda\}$.  Given an $\NC$-tree $\Tcal$, by
$\ndweak{\Tcal}{\Lambda}$ we denote the $\NC$-tree obtained by adding
all the formulas in $\Lambda$ to the context of every sequent
occurring in $\Tcal$ (if $\Lambda=\{A\}$, we simply write
$\ndweak{\Tcal}{A}$).  Note that, if $\Tcal$ is an $\NC$-derivation,
then $\ndweak{\Tcal}{\Lambda}$ is an $\NC$-derivation as well.  To
better display how proofs are combined, we add to $\NC$ the rule
\[
\small
\AXC{$\seqncdown{\G}{H_1}$
\hspace{2ex}$\cdots$\hspace{2ex}
$\seqncdown{\G}{H_n}$\hspace{2ex}
$\seqncup{\G,H_1,\dots,H_n}{A}$} 
% -------------------------
\RightLabel{$\ruleCUT$}
\UIC{$\seqncup{\G}{A}$}
\DP
\]
This rule is admissible in $\NCR$ and, as the reader can easily check,
its applications can be removed using the following strategy. Let
$\Dcal$ be the $\NC$-derivation 
\[
\small
\AXC{$\Dcal_1$}
\noLine
\UIC{$\seqncdown{\G}{H_1}$} 
\AXC{$\cdots$}
\AXC{$\Dcal_n$}
\noLine
\UIC{ $\seqncdown{\G}{H_n}$}
\AXC{$\Ecal$}
\noLine
\UIC{$\seqncup{\G,H_1,\dots,H_n}{A}$} 
% -------------------------
\RightLabel{$\ruleCUT$}
\QIC{$\s=\seqncup{\G}{A}$}
\DP
\]
and let us  assume that the $\NC$-derivations
$\Dcal_1,\dots,\Dcal_n,\Ecal$
are already $\ruleCUT$-free.  
We get a  $\ruleCUT$-free derivation of $\s$ by
performing the following steps.

\begin{enumerate}[label=(\arabic*),ref=(\arabic*)]

\item\label{cut1} In $\Ecal$, for every $1\leq i\leq n$, we remove the
  occurrences of $H_i$ from the contexts of the sequents, except the
  occurrences of $H_i$ corresponding to hypothesis introduced by an
  application of $\botcl$, $\lor E$, $\to I$.  At the end of this
  step, we get a $\ruleCUT$-free $\NC$-tree $\Ecal'$ with root sequent
  $\sigma$. Note that $\Ecal'$ might contain open leaves of the form
  $\s_i=\seqncdown{\G,\G'}{H_i}$, with $H_i\not\in\G\cup\G'$.
 

\item\label{cut2}
To turn $\Ecal'$ into a $\ruleCUT$-free  $\NC$-derivation of $\s$,
we replace every leaf of the kind $\s_i$ 
with the $\NC$-derivation  $\ndweak{\Dcal_i}{\Gamma'}$. 
\end{enumerate}


\noindent
Let $\Dcal$ be an $\NCR$-derivation of $\sigma$; we define the map
$\phi$ so to match the following properties:

\begin{enumerate}[label=(\roman*),ref=(\roman*)]
\item\label{phii1} If $\s=\sequp{\Gamma}{A}{\Delta}$, then
  $\phi(\Dcal)$ is an $\NC$-derivation of $\seqncup{\Gamma,\neg
    \Delta}{A}$.
   
\item\label{phii2}
  Let $\s=\seqdown{\Gamma}{H}{A}{\Delta}{\Theta}$ and $K\in \{A\}\cup\Theta$.\\
  Then $\phi(\Dcal,K)$ is an $\NC$-derivation of
  $\seqncdown{H,\Gamma,\neg \Delta}{K}$\footnote{To be
  more precise,
the translation of $\NCR$-derivations of $\up$-sequents
    (case~\ref{phii1}) should be denoted
by $\phi(\Dcal,A)$; we simply write $\phi(\Dcal)$ since in this
    case $\Theta$ is not defined.}.
\end{enumerate}
  

\noindent
The key point in the translation is that the formulas in $\Delta$ are
transferred to the left by applying the classical rule $\botcl$, so
that they can be used at any successive step to mimic the restart
rules.  We define $\phi$ by induction on the structure of $\Dcal$, by
cases on the root rule $\Rcal$ of $\Dcal$.

\begin{itemize}
  %%% TRANS ID
\item  $\Rcal=\ruleID$. Let
  \[\small
  \AXC{}
  \RightLabel{$\ruleID$}
  \LeftLabel{$\Dcal\;=\;$}
  \UIC{$\seqdown{\G}{H}{H}{\Delta}{}$}
  \DP
  \]
  Then $\phi(\Dcal)$ is
  \[\small
  \AXC{}
  \RightLabel{$\ruleID$}
  \UIC{$\seqncdown{H,\G,\neg \D}{H}$}
  \DP
  \]
  \smallskip
  
%%% TRANS COERC
\item  $\Rcal=\coerc$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\seqdown{\G_H}{H}{p}{p,\D}{\Theta}$}
  % ----
  \RightLabel{$\coerc$}
  \LeftLabel{$\Dcal\;=\;$}
  \UIC{$\sequp{H,\G}{p}{\D}$}
  \DP 
  \]
  Then $\phi(\Dcal)$ is (where $\{H\}\cup\G=\{H\}\cup\G_H$)
  \[\small
  \AXC{}
  \RightLabel{$\ruleID$}
  \UIC{$\seqncdown{H,\G,\neg p,\neg \D}{\neg p}$}
  % \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  \AXC{$\phi(\Dcal_0,p)$}
  \noLine
  \UIC{$\seqncdown{H,\G_H,\neg p,\neg \D}{p}$}
  % ---------------------------------------------
  \RightLabel{$\coerc$}
  \UIC{$\seqncup{H,\G,\neg p,\neg \D}{p}$}
  % =========================================
  \RightLabel{$\to E$}
  \BIC{$\seqncdown{H,\G,\neg p,\neg \D}{\bot}$} 
  % ---------------------------------------------
  \RightLabel{$\botcl$}
  \UIC{$\seqncup{H,\G,\neg \D}{p}$} 
  \DP
  \]
  \smallskip
  
%%% FALSE ELIMINATION 
\item  $\Rcal=\botint$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\seqdown{\G_H}{H}{\bot}{F,\D}{\Theta}$}
  % ----
  \RightLabel{$\botint$}
  \LeftLabel{$\Dcal\;=\;$}
  \UIC{$\sequp{H,\G}{F}{\D}$}
  \DP 
  \]
  Then $\phi(\Dcal)$ is (where $\{H\}\cup\G=\{H\}\cup\G_H$)
  \[\small
  \AXC{$\phi(\Dcal_0,\bot)$}
  \noLine
  \UIC{$\seqncdown{H,\G_H,\neg F,\neg \D}{\bot}$}
  % ---------------------------------------------
  \RightLabel{$\botcl$}
  \UIC{$\seqncup{H,\G,\neg \D}{F}$}
  \DP
  \]
  \smallskip


  %%% TRANS RESTART PRIME
\item  $\Rcal=\ruleRP$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\seqdown{\G_H}{H}{p}{F,p,\D}{\Theta}$}
  % ----
  \RightLabel{$\ruleRP$}
  \LeftLabel{$\Dcal\;=\;$}
  \UIC{$\sequp{H,\G}{F}{p,\D}$}
  \DP 
  \]
  Then $\phi(\Dcal)$ is (where $\{H\}\cup\G=\{H\}\cup\G_H$)
  \[\small
  \AXC{}
  \RightLabel{$\ruleID$}
  \UIC{$\seqncdown{H,\G,\neg F,\neg p,\neg \D}{\neg p}$}
  % \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  \AXC{$\phi(\Dcal_0,p)$}
  \noLine
  \UIC{$\seqncdown{H,\G_H,\neg F,\neg p,\neg \D}{p}$}
  % -------------------------------------------------
  \RightLabel{$\coerc$}
  \UIC{$\seqncup{H,\G,\neg F,\neg p,\neg \D}{p}$}
  % =========================================
  \RightLabel{$\to E$}
  \BIC{$\seqncdown{H,\G,\neg F,\neg p,\neg \D}{\bot}$} 
  % ---------------------------------------------
  \RightLabel{$\botcl$}
  \UIC{$\seqncup{H,\G ,\neg p,\neg \D}{F}$} 
  \DP
  \]
  \smallskip
  
  %%% TRANS RESTART COMPOSED
\item  $\Rcal=\ruleRC$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\sequp{\G}{D}{F,\D_D}$}
  % ----
  \RightLabel{$\ruleRC$}
  \LeftLabel{$\Dcal\;=\;$}
  \UIC{$\sequp{\G}{F}{D,\D}$}
  \DP 
  \]
  Then $\phi(\Dcal)$ is (where $\{\neg D\}\cup\neg\D=\{\neg D\}\cup\neg(\D_D)$):
  \[\small
  \AXC{}
  \RightLabel{$\ruleID$}
  \UIC{$\seqncdown{\neg D,\G,\neg F,\neg \D}{\neg D}$}
  % \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  \AXC{$\ndweak{\phi(\Dcal_0)}{\neg D}$}
  \noLine
  \UIC{$\seqncup{\neg D,\G,\neg F,\neg (\D_D)}{D}$}
  % ======================================================
  \RightLabel{$\to E$}
  \BIC{$\seqncdown{\neg D,\G,\neg F,\neg \D}{\bot}$} 
  % -----------------------------------------------------
  \RightLabel{$\botcl$}
  \UIC{$\seqncup{\neg D,\G,\neg \D}{F}$} 
  \DP
  \]
  \smallskip
  
  %%% TRANS AND I
\item $\Rcal=\land I$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\sequp{\G}{A}{\D}$}
  % \\\\\\
  \AXC{$\Dcal_1$}
  \noLine
  \UIC{$\sequp{\G}{B}{\D}$}
  % ===========================
  \LeftLabel{$\Dcal=$}
  \RightLabel{$\land I$}
  \BIC{$\sequp{\G}{A\land B}{\D}$}
  \DP 
  \]
  Then $\phi(\Dcal)$ is 
  \[\small
  %%%%%%%%%%%%%%%% 
  \AXC{$\phi(\Dcal_0)$}
  \noLine
  \UIC{$\seqncup{\G,\neg \D}{A}$}
  % \\\\\\\\\\\\\\\\\\\\\\\\
  \AXC{$\phi(\Dcal_1)$}
  \noLine
  \UIC{$\seqncup{\G,\neg \D}{B}$}
  % ===========================
  \RightLabel{$\land I$}
  \BIC{$\seqncup{\G,\neg \D}{A\land B}$}
  \DP 
  %%%%%%%%%%%%%%% 
  \]
  \smallskip
  
  %%% TRANS AND E0
\item $\Rcal=\land E_k$. Let
  \[\small
  \AXC{$\Dcal'$}
  \noLine
  \UIC{$\seqdown{\G}{H}{A_0 \land A_1}{\D}{\Theta}$}
  % ----------------------------------
  \RightLabel{$\land E_k$}
  \LeftLabel{$\Dcal\;=\;$}
  \UIC{$\seqdown{\G}{H}{A_k}{\D}{A_{1-k},\Theta}$}
  \DP 
  \]
  For $K\in\Theta$, we set $\phi(\Dcal, K) = \phi(\Dcal',
  K)$. Moreover for $k\in\{0,1\}$, $\phi(\Dcal,A_k)$ is:
  \[\small
  \AXC{$\phi(\Dcal',A_0\land A_1)$}
  \noLine
  \UIC{$\seqncdown{H,\G,\neg \Delta}{A_0 \land A_1}$}
  % ----------------------------------
  \RightLabel{$\land E_k$}
  \UIC{$\seqncdown{H,\G,\neg \Delta}{A_k}$}
  \DP
  \]
  \smallskip

  %%% TRANS OR I
\item  $\Rcal=\lor I$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\sequp{\G}{A}{B,\D}$}
  % ----
  \RightLabel{$\lor I$}
  \LeftLabel{$\Dcal\;=\;$}
  \UIC{$\sequp{\G}{K}{\D}$}
  \DP 
  \qquad K= A\lor B
  \]
  Then $\phi(\Dcal)$ is:
  \[\small
  %%%%%%%%%%%%%%%%%%%%%%%%% 
  \AXC{}
  \RightLabel{$\ruleID$}
  \UIC{$\seqncdown{\neg K,\G,\neg \D}{\neg K}$}
  % \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  \AXC{}
  \RightLabel{$\ruleID$}
  \UIC{$\seqncdown{\neg K,\G,\neg B,\neg \D}{\neg K}$}
  % \\\\\\\\\\\\\\\\\\\
  \AXC{$\ndweak{\phi(\Dcal_0)}{\neg K}$}
  \noLine
  \UIC{$\seqncup{\neg K,\G,\neg B,\neg \D}{A}$}
  % \UIC{$\seqncup{\neg K,\G,\neg \D}{A}$}
  % ---------------------------------------------
  \RightLabel{$\lor I_0$}
  \UIC{$\seqncup{\neg K,\G,\neg B,\neg \D}{K}$}
  % ======================
  \RightLabel{$\to E$}
  \BIC{$\seqncdown{\neg K,\G,\neg B,\neg \D}{\bot}$}
  % ---------------------------------------------
  \RightLabel{$\botcl$}
  \UIC{$\seqncup{\neg K,\G,\neg \D}{B}$}
  % ---------------------------------------------
  \RightLabel{$\lor I_1$}
  \UIC{$\seqncup{\neg K,\G,\neg \D}{K}$}
  % ======================================================
  \RightLabel{$\to E$}
  \BIC{$\seqncdown{\neg K,\G,\neg \D}{\bot}$}
  % ---------------------------------------------
  \RightLabel{$\botcl$}
  \UIC{$\seqncup{\G,\neg \D}{K}$}
  \DP
  \]
  \smallskip
  
  %%% TRANS OR E
\item  $\Rcal=\lor E$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\seqdown{\G_H}{H}{A\lor B}{F,\D}{\Theta}$}
  % /////
  \AXC{$\Dcal_1$}
  \noLine
  \UIC{$\sequp{A,\G_H,\Theta}{F}{\D}$}
  % /////
  \AXC{$\Dcal_2$}
  \noLine
  \UIC{$\sequp{B,\G_H,\Theta}{F}{\D}$}
  %% =============================================
  \RightLabel{$\lor E$}
  \LeftLabel{$\Dcal\;=\;$}
  \TIC{$\sequp{H,\G}{F}{\D}$}
  \DP 
  \]
  Let $\Theta=\{K_1,\dots,K_n\}$, then $\phi(\Dcal)$ is (where
  $\{H\}\cup\G=\{H\}\cup\G_H$):
  
  \[\small
  \AXC{$\phi(\Dcal_0,A\lor B)$}
  \noLine
  \UIC{$\seqncdown{H,\G_H,\neg \D}{A\lor B}$}
  % /////
  \AXC{$\Ecal_1$}
  \noLine
  \UIC{$\seqncup{A,H,\G_H,\neg \D}{F}$}
  % /////
  \AXC{$\Ecal_2$}
  \noLine
  \UIC{$\seqncup{B,H,\G_H,\neg \D}{F}$}
  %% =============================================
  \RightLabel{$\lor E$}
  \TIC{$\seqncup{H,\G,\neg \D}{F}$}
  \DP 
  \]
  where $\Ecal_1$ is the derivation
  \[\small
  \AXC{$\ndweak{\phi(\Dcal_0,K_1)}{A}$}
  \noLine
  \UIC{$\seqncdown{A,H,\G_H,\neg \D}{K_1}$}
  % /////////////////////////
  \AXC{$\dots$}
  % /////////////////////////
  \AXC{$\ndweak{\phi(\Dcal_0,K_n)}{A}$}
  \noLine
  \UIC{$\seqncdown{A,H,\G_H,\neg \D}{K_n}$}
  % //////////////////
  \AXC{$\ndweak{\phi(\Dcal_1)}{H}$}
  \noLine
  \UIC{$\seqncup{H,A,\G_H,\Theta,\neg \D}{F}$}
  % ======================================
  \RightLabel{$\ruleCUT$}
  \QuaternaryInfC{$\seqncup{A,H,\G_H,\neg \D}{F}$}
  \DP
  \]
  and $\Ecal_2$ is defined analogously.
  \smallskip
  
\item  $\Rcal=\to I$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\sequp{A,\G}{B}{\D}$}
  %% =============================================
  \RightLabel{$\to I$}
  \LeftLabel{$\Dcal\;=\;$}
  \UIC{$\sequp{\G}{A\to B}{\D}$}
  \DP 
  \]
  \smallskip The derivation $\phi(\Dcal)$ is
  \[\small
  \AXC{$\phi(\Dcal_0)$}
  \noLine
  \UIC{$\seqncup{A,\G,\neg \D}{B}$}
  %% =============================================
  \RightLabel{$\to I$}
  \UIC{$\seqncup{\G,\neg \D}{A\to B}$}
  \DP 
  \]

\item  $\Rcal=\to E$. Let
  \[\small
  \AXC{$\Dcal_0$}
  \noLine
  \UIC{$\seqdown{\G}{H}{A\to B}{\D}{\Theta}$}
  % /////
  \AXC{$\Dcal_1$}
  \noLine
  \UIC{$\sequp{\G,\Theta}{A}{\D}$}
  %% =============================================
  \RightLabel{$\to E$}
  \LeftLabel{$\Dcal\;=\;$}
  \BIC{$\seqdown{\G}{H}{B}{\D}{\Theta}$}
  \DP 
  \]
  Let $\Theta=\{K_1,\dots,K_n\}$. For $K\in\Theta$, we set
  $\phi(\Dcal, K) = \phi(\Dcal_0, K)$.  The derivation $\phi(\Dcal,
  B)$ is
  \[\small
  \AXC{$\phi(\Dcal_0,A\to B)$}
  \noLine
  \UIC{$\seqncdown{H,\G,\neg \D}{A\to B}$}
  % /////////////////////////////////////////////////
  \AXC{$\dots$}
  % /////////////////////////
  \AXC{$\phi(\Dcal_0,K_i)$}
  \noLine
  \UIC{$\seqncdown{H,\G,\neg \D}{K_i}$}
  % //////////////////
  \AXC{$\dots$}
  % /////////////////////////
  \AXC{$\ndweak{\phi(\Dcal_1)}{H}$}
  \noLine
  \UIC{$\seqncup{H,\G,\Theta,\neg \D}{A}$}
  % ======================================
  \RightLabel{$\ruleCUT$}
  \QuaternaryInfC{$\seqncup{H,\G,\neg \D}{A}$}
  % ==================================================
  \RightLabel{$\to E$}
  \BIC{$\seqncdown{H,\G,\neg \D}{B}$}
  \DP
  \]
\end{itemize}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "app"
%%% End:
