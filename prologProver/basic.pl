%###############    BASIC  #######################



:- module(basic, [
	      % OPERATORS
		%  op(50, fy, #),  % #f represents false
		%  op(100, fy, ~),   % not
		%  op(150, xfy, &),  % and
		%  op(200, xfy, v),  % or
		%  op(300, xfy, '-->'),  % implies
		%  op(250, xfy, '<-->'), % iff
		%  op(600, xfx, '==>'),   % sequent arrow
	       % PREDICATES
		  is_propVar/1,
		  is_prime/1,
		  is_disj/1,
		  is_primeOrDisj/1,
	          is_posSubf/2,
	          get_posSubf_form/2,
	          get_posSubf_list/2,
		  tr_formula/2,
		  tr_formula_list/2,
		  pitpToProver/2
	      ]
	   ).

:- use_module(syntax).


%% isPropVar(+A)
%
%   Check if A is a propositional variable



is_propVar(A) :- atom(A).


% is_Prime(+A)
%
%  Check if A is prime, namely:
%
%   A ::=  PropVar | #f

is_prime(A) :- A = #f ;   is_propVar(A).

% is_disj(A)
%
%  Check if A  is a disjunction A0 V A1

is_disj(_A0 v _A1).


% is_PrimecOrDisj(+A)
%
%  Check if A is prime or a disjunction, namely:
%
%  A = PropVar | #f  | A0 v A1

is_primeOrDisj(A) :- is_prime(A) ; is_disj(A).



% is_posSubf(?PF,+A)
%
%  PF is a positive subformula of A


is_posSubf(PF,PF).
is_posSubf(PF, A & B) :- is_posSubf(PF,A) ;  is_posSubf(PF,B).
is_posSubf(PF, _A -->  B) :- is_posSubf(PF,B).

% get_posSubf_form(+A,-PosSf)
%
% PosSubf = { PF | PF is a positive subformula of A }

get_posSubf_form(A,PosSf) :-
	setof( PF, is_posSubf(PF,A), PosSf).

% get_posSubf_list(+Gamma,-PosSf)
%
% PosSubf = { PF | PF is a positive subformula of o formula A in Gamma }

get_posSubf_list([],[]).
get_posSubf_list([ A | Gamma],PosSf) :-
        get_posSubf_form(A,PosSfA),
	get_posSubf_list(Gamma,PosSf1),
        union(PosSfA,PosSf1,PosSf).


% tr_formula(+A,-F)
%
% Translate the formula A eliminating the defined operators



tr_formula(At,At) :- is_prime(At), !.


tr_formula(~A , F --> #f  ) :- !,
	tr_formula(A, F).


tr_formula(A0 & A1, F0 & F1) :- !,
	tr_formula(A0,F0),
	tr_formula(A1,F1).


tr_formula(A0 v A1, F0 v F1) :- !,
	tr_formula(A0,F0),
	tr_formula(A1,F1).

tr_formula(A0 --> A1, F0 --> F1) :- !,
	tr_formula(A0,F0),
	tr_formula(A1,F1).

tr_formula(A0 <--> A1, (F0 --> F1) & (F1 --> F0) ) :- !,
	tr_formula(A0,F0),
	tr_formula(A1,F1).

% Error

tr_formula(A, _) :-
	writeln('*** INPUT ERROR ***'),
	write(A),
	writeln(' is not a formula'),
	abort.


% tr_formula_list(+Gamma,-Gamma1)
%
% Translate all the formulas in Gamma eliminating the defined operators

tr_formula_list([],[]).
tr_formula_list([A|AList],[F|FList]) :-
	tr_formula(A,F),
	tr_formula_list(AList,FList).






%%  translation from pitp syntax to this syntaxt

pitpToProver(non(A) , A1 -->  #f) :- !,
	pitpToProver(A,A1).

pitpToProver(and(A,B) , A1 & B1):- !,
	pitpToProver(A,A1),pitpToProver(B,B1).

pitpToProver(or(A,B) , A1 v B1) :- !,
	pitpToProver(A,A1),pitpToProver(B,B1).

pitpToProver(im(A,B) , A1 -->  B1):- !,
	pitpToProver(A,A1),pitpToProver(B,B1).

pitpToProver(equiv(A,B) , ( A1 --> B1) & (B1 --> A1) ):- !,
	pitpToProver(A,A1),pitpToProver(B,B1).

pitpToProver(A,A).
























































