:- module(syntax, [
	      % OPERATORS
		  op(50, fy, #),  % #f represents false
		  op(100, fy, ~),   % not
		  op(150, xfy, &),  % and
		  op(200, xfy, v),  % or
		  op(300, xfy, '-->'),  % implies
		  op(250, xfy, '<-->'), % iff
	          op(550, xfy, '@'), % separator
		  op(600, xfx, '==>'),   % sequent arrow
	          op(600, xfx, '=/=>'),   % refutation sequent arrow
	          op(600, xfx, '>>')   % reduction
	      ]
	   ).
