%#################  UTILITY  ######################%






:- module(util, [
		 proof_to_latex/3,  % trees_to_latex(+Proof,+Dir,+Latex_file)
		 generate_pdf/2,    % generate_pdf(+Dir,+Latex_file)
	         add_labels/2       %add_labels(+Proof, -LabelledProof
              ]
           ).

:- use_module(prover, [
		%buildProof/3, % buildProof(+Seq,-Proof,-ProofType : {proof, ref})
		%search_main/2,
		is_proof/1,
	        is_ref/1
	      ]
	   ).

:- use_module(syntax).

%:- use_module(readutil).


preamble_latex_file("preamble.tex").  %%% File containing the preamble for the Latex file


%%%%%%%%     LATEX   RENDERING      %%%%%%%%%%%%%%%%%%%%%%%
%
%
%

% proof_to_latex(+Proof, +Dir, +Latex_file)
% Proof is a proof or a labelled proof or  a ref


proof_to_latex(Proof,Dir,Latex_file) :-
	atomic_list_concat([Dir, '/',Latex_file], File_path),
	tell(File_path), % open File_path
	write_preamble,
	write_proof(Proof),
	writeln('\\end{document}'),
	told, % close open files
	write('*** '), write(File_path), writeln(' generated'),!.


proof_to_latex(_,_,_) :-  % errors
	told, % close open files
	writeln('***  tree_to_latex errors   ***'),
	abort.


%  generate_pdf(+Dir,+Latex_file)
%
%  Generate pdf of Dir/Latex_file

generate_pdf(Dir,Latex_file) :-
	file_name_extension(Base_name, '.tex', Latex_file),
	% Base_name: base name of Latex_file
	atomic_list_concat([Dir, '/', Base_name], Base_name_path),
	atomic_list_concat([Dir, '/', Latex_file], Latex_file_path),
	atomic_list_concat(['pdflatex -interaction=batchmode  -output-directory ', Dir, ' ', Latex_file_path],
			   Pdflatex_cmd),
	write('*** '),
	writeln(Pdflatex_cmd),
	shell(Pdflatex_cmd,Status), % exec Pdflatex_cmd, Status is the outcome
	(
	Status == 0 ->
	  atomic_list_concat(['rm -f ', Base_name_path, '.aux ', Base_name_path, '.log '] , Clean_cmd),
	  write('*** '),
	  writeln(Clean_cmd),
	  shell(Clean_cmd),
	  write('***  '),
	 atomic_list_concat([Base_name_path,'.pdf'],Pdf_file_path),
	 write(Pdf_file_path),
	 writeln(' generated')
	; % pdflatex errors
	  told, % close open files
	  writeln('*** pdflatex errors'),
	  abort
	).

%========================================================

%%  Add number labels to sequents of proofs
%  according with the order they are processes

add_labels(ProofIn,LabProof) :-
	  add_labels(ProofIn,0,LabProof,_).



% add_label_up(+Proof,+NIn,-LabelledProof,-NOut)
%
%  Nin : first label to be used
%  NOut: last label


add_labels(proof(andI,Seq, [P1,P2]), NIn,
	     proof(andI,Seq @ lab(NIn), [LabP1,LabP2] ), NOut ):- !,
	NIn1 is NIn +1,
	add_labels(P1,NIn1, LabP1,NOut1),
	NIn2 is NOut1 + 1,
	add_labels(P2,NIn2, LabP2,NOut).


add_labels(proof(orI,Seq, [P]), NIn,
	     proof(orI,Seq @ lab(NIn), [LabP] ), NOut ):- !,
	NIn1 is NIn+1,
	add_labels(P,NIn1, LabP,NOut).


add_labels(proof(implI,Seq, [P]), NIn,
	     proof(implI,Seq @ lab(NIn), [LabP] ), NOut ):- !,
	NIn1 is NIn+1,
	add_labels(P,NIn1, LabP,NOut).

add_labels(proof(coerc,Seq, [P]), NIn,
	     proof(coerc,Seq @ lab(NIn), [LabP] ) , NOut):- !,
	NIn1 is NIn +1,
	add_labels(P,NIn1, LabP,NOut).

add_labels(proof(botE,Seq, [P]), NIn,
	     proof(botE,Seq @ lab(NIn), [LabP] ), NOut ):-  !,
	NIn1 is NIn + 1,
	add_labels(P,NIn1, LabP,NOut).

add_labels(proof(restp,Seq, [P]), NIn,
	     proof(restp,Seq @ lab(NIn), [LabP] ) , NOut):-  !,
	NIn1 is NIn+1,
	add_labels(P,NIn1, LabP,NOut).



add_labels(proof(restc,Seq, [P]), NIn,
	     proof(restc,Seq @ lab(NIn), [LabP] ) , NOut):-  !,
	NIn1 is NIn +1,
	add_labels(P,NIn1, LabP,NOut).

add_labels(proof(orE,Seq, [P1,P2,P3]), NIn,
	     proof(orE,Seq @ lab(NIn1), [LabP1,LabP2,LabP3] ), NOut ):-  !,
	add_labels(P1,NIn, LabP1,NOut1),
	NIn1 is NOut1 + 1,
	NIn2 is NIn1 + 1,
	add_labels(P2,NIn2, LabP2,NOut2),
        NIn3 is NOut2 +1,
	add_labels(P3,NIn3, LabP3,NOut).


add_labels(proof(id,Seq, []), NIn,
	       proof(id,Seq @ lab(NIn), [] ) , NIn) :- !.


add_labels(proof(andE0,Seq, [P]), NIn,
	       proof(andE0,Seq @ lab(NOut), [LabP] ), NOut ):-  !,
	 add_labels(P,NIn, LabP,NOut1),
	 NOut is NOut1 +1.



add_labels(proof(andE1,Seq, [P]), NIn,
	       proof(andE1,Seq @ lab(NOut), [LabP] ), NOut ):-  !,
	  add_labels(P,NIn, LabP,NOut1),
	  NOut is NOut1 +1.



add_labels(proof(implE,Seq, [P1,P2]), NIn,
	       proof(implE,Seq @ lab(NIn1), [LabP1,LabP2] ), NOut ):-  !,
	add_labels(P1,NIn, LabP1,NOut1),
        NIn1 is NOut1 +1,
	NIn2 is NIn1 + 1,
	add_labels(P2,NIn2, LabP2,NOut).



%=========================================================

%%     RULE NAMES

write_rule(id) :-  !, write('\\ruleID').
write_rule(botE) :-  !, write('\\bot E').
write_rule(coerc) :-  !, write('\\coerc').
write_rule(andI) :-  !, write('\\land I').
write_rule(andE0) :-  !, write('\\land E_0').
write_rule(andE1) :-  !, write('\\land E_1').
write_rule(andE) :-  !, write('\\land E').
write_rule(orI0) :- !, write('\\lor I_0').
write_rule(orI1) :- !, write('\\lor I_1').
write_rule(orI) :- !, write('\\lor I').
write_rule(orE) :- !, write('\\lor E').
write_rule(implI) :- !, write('\\to I').
write_rule(implI1) :- !, write('\\to I_1').
write_rule(implI2) :- !, write('\\to I_2').
write_rule(implE) :- !, write('\\to E').
write_rule(irr) :- !, write('\\ruleIRR').
write_rule(restp) :- !, write('\\ruleRP').
write_rule(restc) :- !, write('\\ruleRC').

% Error: not defined R
write_rule(R) :- write('\\mbox{!!! ' ), write(R), write('}').


%##########   WRITE TREES ############



write_preamble :-
	preamble_latex_file(File),  %% file containing the preamble
	open(File,  read, Stream),
	read_stream_to_codes(Stream,CodeList),
	forall( member(Code, CodeList), (char_code(Char,Code), write(Char))),
	close(Stream).




%%% Write formulas

is_atomic_or_negated_formula(A):- atom(A).
is_atomic_or_negated_formula(#f).
is_atomic_or_negated_formula(_A --> #f).

% write_formula(+F:formula)


write_formula(#f) :- !,
        write('\\bot').

write_formula(F):-
        atom(F), !,
        write(F).

write_formula( A --> #f) :- !,
        (
         is_atomic_or_negated_formula(A) ->
           write('\\neg '),  write_formula(A)
        ;
          write('\\neg '), write('('),   write_formula(A), write(')')
        ).


write_formula( (A --> B) & (B-->A) ) :- !,
         ( is_atomic_or_negated_formula(A) ->
            write_formula(A)
         ;
           write('('), write_formula(A), write(')')
        ),
         write('\\leftrightarrow '),
         ( is_atomic_or_negated_formula(B) ->
            write_formula(B)
         ;
           write('('), write_formula(B), write(')')
        ).



write_formula(F):-  % F is a binary formula
        F =.. [Op, A , B], !,
        ( is_atomic_or_negated_formula(A) ->
            write_formula(A)
         ;
           write('('), write_formula(A), write(')')
        ),
        write_op(Op),
        ( is_atomic_or_negated_formula(B) ->
            write_formula(B)
         ;
           write('('), write_formula(B), write(')')
        ).

write_op('&') :-  write(' \\land ').
write_op('v') :-  write(' \\lor ').
write_op('-->') :- write(' \\to ').




% write_formula_list(+L : list of formulas)

write_formula_list([]). % :- write('\\emptyset').
write_formula_list([A]) :- write_formula(A), !.
write_formula_list([A | L]) :-
	write_formula(A), write(',\\,'),
	write_formula_list(L).


% big_union( +L , -Gamma)
%
%  L: list of lists of formula
%  Gamma =  union of the lists in L

%big_union([],[]).
%big_union([ I] , I).
%big_union([ I | L ], U) :-
%	big_union(L, V),
%	union(I,V,U).

write_sequent(( Gamma  ==> up(A) @ Delta  )@ lab(N)) :- !,
	write('\\labsequp{'),
	%sort(Gamma,GammaOrd),
	write_formula_list(Gamma),
	write('}{'),
	write_formula(A),
	write('}{'),
	write_formula_list(Delta),
	write('}{'),
	write(N),
	write('}').


write_sequent( Gamma  ==> up(A) @ Delta	) :- !,
	write('\\sequp{'),
	%sort(Gamma,GammaOrd),
	write_formula_list(Gamma),
	write('}{'),
	write_formula(A),
	write('}{'),
	write_formula_list(Delta),
	write('}').





write_sequent( Gamma  =/=> up(A) @ Delta ) :- !,
	write('\\sequp{'),
	write_formula_list(Gamma),
	write('}{'),
	write_formula(A),
	write('}{'),
	write_formula_list(Delta),
	write('}').


write_sequent( (Gamma @ H  ==> down(A) @ Delta @ Theta) @ lab(N)) :- !,
	write('\\labseqdown{'),
	write_formula_list(Gamma),
	write('}{'),
	write_formula(H),
	write('}{'),
	write_formula(A),
	write('}{'),
	write_formula_list(Delta),
	write('}{'),
	write_formula_list(Theta),
	write('}{'),
	write(N),
	write('}').


write_sequent( Gamma @ H  ==> down(A) @ Delta @ Theta) :- !,
	write('\\seqdown{'),
	write_formula_list(Gamma),
	write('}{'),
	write_formula(H),
	write('}{'),
	write_formula(A),
	write('}{'),
	write_formula_list(Delta),
	write('}{'),
	write_formula_list(Theta),
	write('}').

write_sequent( Gamma @ H  =/=> down(A) @ Delta @ Theta) :- !,
	write('\\seqdown{'),
	write_formula_list(Gamma),
	write('}{'),
	write_formula(H),
	write('}{'),
	write_formula(A),
	write('}{'),
	write_formula_list(Delta),
	write('}{'),
	write_formula_list(Theta),
	write('}').

write_reduces(H >> A @ Theta) :- !,
	write('\\redto{'),
	write_formula(H),
	write('}{'),
	write_formula(A),
	write('}{'),
	write_formula_list(Theta),
	write('}').



%% write_proof(+Tree : tree)
%

%write_proof(leaf(Sequent)) :- !,
%	write_sequent(Sequent).


write_proof(proof(Rule,  Sequent, Premise_list)) :- !,
        write('\\infer['),
	write_rule(Rule),
	writeln(']{'),
	write_sequent(Sequent),
	writeln('}{'),
	write_proof_premise_list(Premise_list),
	writeln('}').


write_proof(ref(Rule,  Sequent, [H >> A @ Theta ,Ref1])) :- !,
        write('\\infer['),
	write_rule(Rule),
	writeln(']{'),
	write_sequent(Sequent),
	writeln('}{'),
        write_reduces(H >> A @ Theta),
	write(' & '),
	write_proof(Ref1),
	writeln('}').

write_proof(ref(Rule,  Sequent, Premise_list)) :- !,
        write('\\infer['),
	write_rule(Rule),
	writeln(']{'),
	write_sequent(Sequent),
	writeln('}{'),
	write_proof_premise_list(Premise_list),
	writeln('}').





write_proof_premise_list([]).

write_proof_premise_list([Tree]) :- !,
	write_proof(Tree).

write_proof_premise_list([Tree | Tree_list]) :-
	write_proof(Tree),
	write(' & '),
	write_proof_premise_list(Tree_list).




































