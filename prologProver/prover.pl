%############### NDBU  GOAL ORIENTED
%

% #######################



:- module(prover, [
		buildProof/3, % buildProof(+Seq,-Proof,-ProofType : {proof, ref})
		search_main/2,
		is_proof/1,
	        is_ref/1
	      ]
	   ).

:- use_module(basic,[
		  is_propVar/1,
		  is_prime/1,
		  is_disj/1,
		  is_primeOrDisj/1,
		  is_posSubf/2,
	          get_posSubf_form/2,
	          get_posSubf_list/2,
		  tr_formula/2,
		  tr_formula_list/2
		  %pitpToProver/2,
		  ]).

:- use_module(syntax).



%############    SYNTAX  ############
%
%
%  *  Formula A,B ::=  P | #f |  A & B | A v B | A --> B
%
%     where:
%     -    P is a Prolog atom and represents a propositional variable
%     -    #f represents false
%
%    Prime formulas: P | #f
%
%
%  *  up_sequent     ::=   Gamma ==> up(A) @ Delta
%     down-sequent   ::=  Gamma @ H ==> down(A) @ Delta @ Theta
%
%    where:
%     -   A, H  are formulas
%     -  Gamma, Delta Theta are lists of formulas
%
%    In refutations, we use the arrow '=/=>'
%
%  *  reduction ::=  H >> K @ Theta
%
%     where:
%      -  H, K are formulas
%      -  Theta is a list of formulas
%
%
%  *  Proof ::=   proof(RootRule,RootSequent,PremiseList)
%
%     Let PremiseList = [Pr_1 ... Pr_n ] be a list of proofs.
%     Then, Proof represents the derivation
%
%
%	       Pr_1  .....  Pr_n
%	      ------------------- RootRule
%		RootSequent
%
%
%
%  *  Ref ::=  ref(RootRule,RootSequent,PremiseList)
%
%     is defined similarly to proofs; the root sequent has arrow '=/=>'.



% is_irrSeq(+ Gamma  ==> up(A) @ Delta)
%
% True iff Gamma  ==> up(A) @ Delta  is an irreducible sequent

is_irrSeq(Gamma  ==> up(A) @ Delta) :-
	union(Delta,[A],Delta1),
	%etof( F, member(F,Gamma)
	get_posSubf_list(Gamma,PosSfGamma),
        % PosSfGamma = { PF | PF is a pos. subfurmula of a formula in Gamma }
        %% COND 1
	forall(member(D,Delta1), is_prime(D)),
	%% COND 2
        forall(member(D,Delta1), not(member(D,PosSfGamma))),
	%% COND 3
	not(member(#f,PosSfGamma)),
	(
	    member(B v C, PosSfGamma) ->
	      (	   member(B,PosSfGamma) ;   member(C,PosSfGamma) )
	       ;
	    true
	).

%%%  is_proof(+Tree), is_ref(+Tree)
%
%   Check if Tree is a proof or a derivation

is_proof(proof(_Rule,_Seq,_PremiseList)).
is_ref(ref(_Rule,_Seq,_PremiseList)).



%##############    MAIN    ####################à

% buildProof( +Gamma ==> up(A), -Proof, -ProofType)
%
%  Proof: a prroof or a refutation of  Gamma ==> up(u,A)
%  ProofType in {proof, ref}



buildProof( Gamma ==> up(A)  , Proof, ProofType) :-
	tr_formula_list(Gamma,Gamma1),
	tr_formula(A,A1),
	up_search(Gamma1==> up(A1) @ [],  Proof),
	(
	    is_proof(Proof) ->
	       ProofType = proof
	        ;
	      assertion(is_ref(Proof)),
	      ProofType = ref
	).



%#########################################################
%#############	 PROOF-SEARCH ALGORITHM     ##############
%#########################################################

%%   **NOTE** :
%%   henceforth we assume that formulas do not contain
%%   derived  operators ~, <-->


%########   GOAL ORIENTED SEARCH #########

% search_main( +Gamma ==> up(A), -Result)
%
%  Result: a derivation or a refutation of Gamma ==> up(A)



search_main( Gamma ==> up(A)  , Result) :-
	up_search( Gamma  ==> up(A) @ [] ,  Result).




% search_main( +Gamma ==> up(A) @ Delta, -Result)
%
%  Result: a derivation or a refutation of Gamma ==> up(A) @ Delta

% F prime
up_search(  Gamma  ==> up(F) @ Delta, Result) :-
	is_prime(F) , !,
        (
	  ( is_propVar(F), member(H,Gamma), is_posSubf(F,H) )  ->
	      up_search_coerc( H, Gamma  ==> up(F) @ Delta, Result)
	 ;
	  ( member(H,Gamma), is_posSubf(#f,H))  ->
	      up_search_botE( H, Gamma  ==> up(F) @ Delta, Result)
	 ;
          (   member(H,Gamma), is_posSubf(A v B,H) ) ->
	     up_search_orE(H, A v B , Gamma  ==> up(F) @ Delta, Result)
	 ;
	    ( member(P,Delta), is_propVar(P), member(H,Gamma), is_posSubf(P,H) ) ->
	     up_search_restPrime(H,P, Gamma  ==> up(F) @ Delta, Result)
	 ;
	  ( member(D,Delta), not(is_prime(D)) ) ->
	     up_search_restComp(D, Gamma  ==> up(F) @ Delta, Result)
	 ;
	 ( member(D,Delta), member(H,Gamma), is_posSubf(D,H) ) ->
	     up_search_restComp(D, Gamma  ==> up(F) @ Delta, Result)
         ; % if none of the above conditions is true, the input sequent is irreducible
	  assertion(is_irrSeq(  Gamma  ==> up(F) @ Delta) ),
	  Result = ref( irr,Gamma  =/=> up(F) @ Delta, [] )
	).


% apply andI
up_search(  Gamma  ==> up(A & B) @ Delta, Result) :-
        up_search(Gamma  ==> up(A) @ Delta, ResUpA), !,
	(
	   is_ref(ResUpA) ->
	      Result = ref(andI, Gamma  =/=> up(A & B) @ Delta, [ResUpA] )
	; % ResUpA is a proof
	    up_search(Gamma  ==> up(B) @ Delta, ResUpB), !,
	    (
		 is_ref(ResUpB) ->
		    Result = ref(andI, Gamma  =/=> up(A & B) @ Delta, [ResUpB] )
		    ;
		    Result = proof(andI , Gamma  ==> up(A & B) @ Delta,  [ResUpA,ResUpB])
	    )
	).


% apply orI
up_search(  Gamma  ==> up(A v B) @ Delta, Result) :-
        union(Delta,[B], BDelta),
	up_search(Gamma  ==> up(A) @ BDelta, ResUp), !,
	(
	   is_proof(ResUp) ->
	      Result = proof(orI , Gamma  ==> up(A v B) @ Delta,  [ResUp])
	      ;
	      Result = ref(orI , Gamma  =/=> up(A v B) @ Delta,  [ResUp])
	).

% apply implI
up_search(  Gamma  ==> up(A-->B) @ Delta, Result) :-
	union(Gamma,[A],AGamma),
        up_search(AGamma  ==> up(B) @ Delta, ResUp), !,
	(
	   is_proof(ResUp) ->
	      Result = proof(implI, Gamma  ==> up(A-->B) @ Delta, [ResUp] )
	;
	      Result = ref(implI, Gamma  =/=> up(A-->B) @ Delta, [ResUp] )
	).

% apply coerc
up_search_coerc(H, Gamma  ==> up(P) @ Delta, Result) :-
	assertion(member(H,Gamma)),
	assertion(is_propVar(P) ),
	assertion(is_posSubf(P,H)),
	subtract(Gamma, [H], Gamma_H),
	union(Delta,[P],PDelta),
	ProofId = proof( id, Gamma_H @ H ==> down(H) @ PDelta @ [], [] ),
	down_expand( ProofId , P, ResDown), !,
	(
	   is_proof(ResDown) ->
	       Result = proof(coerc, Gamma  ==> up(P) @ Delta, [ResDown] )
	    ;
	       ResDown = [Rule, SideCond, RefUp],
	       Result = ref(Rule, Gamma  =/=> up(P) @ Delta,  [SideCond,RefUp] )
	).

% apply botE
up_search_botE(H, Gamma  ==> up(F) @ Delta, Result) :-
	assertion(member(H,Gamma)),
	assertion(is_prime(F) ),
	assertion(is_posSubf(#f,H)),
	subtract(Gamma, [H], Gamma_H),
	union(Delta,[F], FDelta),
	ProofId = proof( id,  Gamma_H @ H ==> down(H) @ FDelta @ [], []),
        down_expand( ProofId ,#f, ResDown), !,
	(
	   is_proof(ResDown) ->
	       Result = proof(botE, Gamma  ==> up(F) @ Delta, [ResDown] )
	    ;
	       ResDown = [Rule, SideCond, RefUp],
	       Result = ref(Rule, Gamma  =/=> up(F) @ Delta, [SideCond,RefUp]   )
	).

% apply orE
up_search_orE(H , A v B , Gamma  ==> up(F) @ Delta, Result) :-
	assertion(member(H,Gamma)),
	assertion(is_prime(F)) ,
	assertion(is_posSubf(A v B,H)),
	subtract(Gamma, [H], Gamma_H),
	union(Delta, [F] , FDelta),
	ProofId = proof(id,  Gamma_H @ H ==> down(H) @ FDelta @ [], []),
        down_expand( ProofId, A v B , ResDown), !,
	(
	  is_proof(ResDown) ->
	    ResDown = proof(_Rule, _Gamma @ _H ==> down(_A) @ _Delta @ Theta   ,_PremiseList),
	    SideCond = H  >>  A v B @ Theta,
            union(Gamma_H,[A|Theta],AThetaGamma_H),
	    up_search(AThetaGamma_H ==> up(F) @ Delta,ResUpA), !,
	    (
		is_ref(ResUpA) ->
		   Result = ref(orE, Gamma  =/=> up(F) @ Delta, [SideCond,ResUpA])
		;
		%%  is_proof(ResDown)  and is_proof(ResUpA)
		    union(Gamma_H,[B|Theta], BThetaGamma_H),
	            up_search(BThetaGamma_H ==> up(F) @ Delta,ResUpB), !,
	             (
		     is_ref(ResUpB) ->
		       Result = ref(orE,Gamma =/=> up(F) @ Delta, [SideCond,ResUpB ])
		     ;
		     %%  is_proof(ResDown), is_proof(ResUpA) and  is_proof(ResUpB)
		     Result = proof(orE, Gamma  ==> up(F) @ Delta, [ResDown,ResUpA,ResUpB])
		   )
	    )
	    ;
	    ResDown = [Rule,SideCond,Ref],
	    Result= ref(Rule, Gamma  =/=> up(F) @ Delta, [ SideCond, Ref])
	).


% apply rest prime
up_search_restPrime(H,P, Gamma  ==> up(F) @ Delta, Result) :-
	assertion(member(H,Gamma)),
	assertion(is_propVar(P) ),
	assertion(is_posSubf(P,H)),
	subtract(Gamma, [H], Gamma_H),
	%subtract(Delta, [P], Delta_P),
	%union(Delta_P,[F],FDelta_P),
	union(Delta,[F],FDelta),
	ProofId = proof( id,  Gamma_H @ H ==> down(H) @ FDelta @ [], [] ),
	down_expand( ProofId , P, ResDown), !,
	(
	   is_proof(ResDown) ->
	       Result = proof(restp, Gamma  ==> up(F) @ Delta, [ResDown] )
	    ;
	       ResDown = [Rule, SideCond, RefUp],
	       Result = ref(Rule, Gamma  =/=> up(F) @ Delta,  [SideCond,RefUp] )
	).

% apply rest comp
up_search_restComp(D, Gamma  ==> up(F) @ Delta, Result) :-
	assertion( not(is_prime(D)) ) ,
	assertion(member(D,Delta)),
	assertion(is_prime(F)),
	subtract(Delta, [D], Delta_D),
	union(Delta_D,[F], FDelta_D),
	up_search(  Gamma  ==> up(D) @ FDelta_D, ResUp), !,
	(
	    is_proof(ResUp) ->
	       Result = proof(restc, Gamma  ==> up(F) @ Delta, [ResUp] )
	    ;
	       Result = ref(restc, Gamma  =/=> up(F) @ Delta, [ResUp] )
	).


% down_expand(+Proof, +K, -Result)
% Assumptions:
%  1)  Proof = proof(R,Gamma @ H ==> down(C) @ Delta @Theta,PremiseList)
%  2)  K = prime | K0 v K1
%  3)  K is a pos. subformula of C
%  Result =
%    a proof of Gamma @ H ==> down(K)
%   OR
%
% a triple ( Rule , RedCond , Ref )
%
%  such that
%
%  -  Ref is a refutation of sigma =  Gamma,Theta =/=> up(A) @ Delta
%  -  RedCond = H >> U @ Theta  is a  reduction condition for sigma
%  -  Rule is the refutation rule having premises  RedCond and Ref
%


% C=K (base)
down_expand(proof(R,Gamma @ H ==> down(C) @ Delta @ Theta,PremiseList), C,
	    proof(R,Gamma @ H ==> down(C) @ Delta @ Theta,PremiseList) ):- !,
	   assertion((not(member(H,Gamma)))),
	   assertion(is_primeOrDisj(C)).

%  C = A & B   apply andE
down_expand(proof(R,Gamma @ H ==> down(A & B) @ Delta @ Theta,PremiseList), K, Result) :-
	 assertion((not(member(H,Gamma)))),
	 assertion(is_primeOrDisj(K)),
	 assertion(is_posSubf(K, A & B )),
	 ProofIn = proof(R,Gamma @ H ==> down(A & B) @ Delta @ Theta, PremiseList),
	 (
	  is_posSubf(K,A) ->
	     union(Theta,[B],BTheta),
	     ProofA = proof( andE0, Gamma @ H ==> down(A) @ Delta @ BTheta , [ProofIn]),
	     down_expand(ProofA, K, Result)
	;    % B must be a pos. subformula of K
	     assertion(is_posSubf(K,B)),
	     union(Theta,[A],ATheta),
	     ProofB = proof( andE1, Gamma @ H ==> down(B) @ Delta @ ATheta , [ProofIn]),
	     down_expand(ProofB, K, Result)
	).



% C = A --> B  apply implE
down_expand(proof(R,Gamma @ H ==> down(A-->B) @ Delta @ Theta,PremiseList), K, Result) :-
	 assertion((not(member(H,Gamma)))),
	 assertion(is_primeOrDisj(K)),
	 assertion(is_posSubf(K, A --> B )),
	 ProofIn = proof(R,Gamma @ H ==> down(A-->B) @ Delta @ Theta,PremiseList),
	 union(Gamma,Theta, GammaTheta),
	 up_search(GammaTheta ==> up(A) @ Delta, ResUp),
	 (
	   is_ref(ResUp) ->
	     Result = [implE,  H >> A-->B  @ Theta, ResUp]
	     ;
	     Proof1 = proof(implE, Gamma @ H ==> down(B) @ Delta @ Theta , [ProofIn, ResUp]),
	     down_expand(Proof1,  K, Result)
	 ).



























