%%%  TESTS
%
%
%
:-use_module(syntax).
:- set_prolog_flag(toplevel_print_options, [max_depth(20)]).

:-use_module(prover,[
		 buildProof/3, % buildProof(+Seq,-Result,-ProofType)
		 %search_main/2,
		 is_proof/1,
		 is_ref/1
	     ]
	    ).

:-use_module(util,[
		 proof_to_latex/3,
		 add_labels/2,
		 generate_pdf/2
	     ]).
%:-use_module(basic).




% SETTING

% set the directory for the latex file containing the proofs
% test_dir(tests).
% test_dir(examples).


test_dir(TestDir) :-
     getenv('HOME', Home),
%     atomic_list_concat( [Home, '/cltests'],TestDir).
          atomic_list_concat( [Home, '/prova'],TestDir).




pp(P) :- P = proof(implI,[]==>up(((p-->q)-->p)-->p)@[],[proof(coerc,[ (p-->q)-->p]==>up(p)@[],[proof(implE,[]@ (p-->q)-->p==>down(p)@[p]@[],[proof(id,[]@ (p-->q)-->p==>down((p-->q)-->p)@[p]@[],[]),proof(implI,[]==>up(p-->q)@[p],[proof(restp,[p]==>up(q)@[p],[proof(id,[]@p==>down(p)@[q]@[],[])])])])])]).






% MAIN

%%% proof

tt(SeqLabel, Result) :-
	get_seq(SeqLabel,Seq),
	buildProof(Seq,Result,_).

%%%   proof with label + latex

test(SeqLabel, Result) :-
	test1(noLabels,SeqLabel, Result, _LatexFile).

testlab(SeqLabel, Result) :-
	test1(addLabels,SeqLabel, Result, _LatexFile).


%%%   proof + latex + pdf

testpdf(SeqLabel, Result)  :-
	test1(noLabels,SeqLabel,Result,LatexFile),
	test_dir(Dir),
	generate_pdf(Dir,LatexFile).

%%%   proof with labels + latex + pdf

testlabpdf(SeqLabel, Result)  :-
	test1(addLabels,SeqLabel,Result,LatexFile),
	test_dir(Dir),
	generate_pdf(Dir,LatexFile).



%%%  First argument:
%%%%  noLabels	  --->   no labels
%%%%  addLabels   --->   add labels to proofs

test1(Labels, SeqLabel, Result,LatexFile) :-
	  get_seq(SeqLabel,Seq),
	  test_dir(Test_dir),
	  check_test_dir(Test_dir),
	  buildProof(Seq,Result,ProofType),
	  (
	     ( ProofType = proof , Labels = addLabels ) ->
	            add_labels(Result,Proof)
	           ;
	     Proof = Result
	 ),
       build_file_name(SeqLabel,ProofType,LatexFile),
       proof_to_latex(Proof,Test_dir,LatexFile).


%% get_seq(+SeqLabel,-Seq)

get_seq(SeqLabel,Seq) :-
	seq(SeqLabel,Seq)  ->
	     true
	  ;
	      %% SeqLabel is not a valid label
	 write('**** Not a valid sequent label: '),
	 writeln(SeqLabel),
	 abort.


check_test_dir(Test_dir) :-
	exists_directory(Test_dir) ->  % create test_dir if it does not exist
	true
	;
	make_directory(Test_dir).

% build_file_name(+SeqLabel,+Proof,+ProofType,-LatexFileName)
%  ProofType: proof of ref

build_file_name(SeqLabel,ProofType,LatexFileName) :-
	ProofType = proof ->
           atomic_list_concat(['proof_',SeqLabel,'.tex'],LatexFileName)
	     ;
	     atomic_list_concat(['ref_',SeqLabel,'.tex'],LatexFileName).



%#####  TESTS

%%%%  IMPLICATION  %%%%%



seq(peirce,   []  ==>  up(A) ) :-
  A = ( (p --> q) --> p) --> p.

seq(im0, [p] ==> up(p) ).

seq(im1, [p, p--> q, q--> r] ==> up(r) ).

seq(im2,  [p-->q ] ==>up(p ) ). % unprovable

seq(im3, [p, p--> q, q--> r] ==> up(t) ). % unprovable


seq(im4, [p--> q, q--> p] ==> up(p) ). % unprovable

seq(im5, [p, p --> ( p --> q)] ==> up(q) ).

seq(im6, [p, p--> q, q--> r] ==>up( t ) ). % unprovable




seq(imax1, [] ==>  up(A) ):-
	A =  p --> ( q --> p).

seq(imax2, [] ==>  up(A)  ):-
	A =  p --> ( p --> q) --> (p -->q).

seq(imax3, [] ==>  up(A)  ) :-
	A = B --> C,
	B = p --> (q -->r),
	C = q --> (p -->r).

seq(imax4, [] ==> up(A)  ) :-
	A = B --> C,
	B = p --> q,
	C = ( q-->r )--> (p -->r).




seq(oli1,   [ c --> a , (c --> a) --> a ]  ==>  up(a) ).

seq(oli2,   [ c --> a , p, p --> ( (c --> a) --> a ) ]  ==> up(a) ). %%%%% LOOP ??


seq(oli3,   [ c  , p, p --> ( c --> a ) ]  ==> up(a) ). %
seq(oli3x,  [c  , p, q --> ( c --> b ) ]  ==> up(a) ). %


seq(oli4,   [ a --> b  ,(a --> b)-->a   ]  ==> up(b) ). %



%%%%  IMPLICATION, BOT  %%%%%

seq(b1, [p, ~p ] ==>  up(r)  ).


seq(b2, [p, p--> q, ~q] ==>up( t ) ).

seq(b3, [p, p--> q, ~s] ==>up( t ) ). % unprovable

seq(b4, [p & ~p ] ==>  up(q)  ).


seq(nnp,   []  ==> up( A ) ) :-
  A = ~ ~p.

seq(nnp1,   []  ==> up( A ) ) :-
  A = ~ ~p --> p.

%%%%  IMPLICATION, BOT , AND %%%%%


seq(andax1,  [] ==> up(A)) :-
	A = a --> (b --> ( a & b) ).

seq(andax2,  [] ==> up(A)) :-
	A = a & b --> a.

seq(andax3,  [a & b & c] ==> up(b)) .




seq(andoli1,   [ (c --> a) &  ((c --> a) --> a) ]  ==>  up(a) ).

seq(andoli2,   [ (c --> a) & p &  ( p --> ( (c --> a) --> a )) ]  ==> up(a) ).


seq(andoli3,   [ c &  p & ( p --> ( c --> a )) ]  ==> up(a) ). %
seq(andoli3x,  [c	&  p &  (q --> ( c --> b )) ]  ==> up(a) ). %


seq(andoli4,   [ A1, A2, A3, b1 --> b2 --> b3 --> c, c --> a ]  ==> up(a) ) :- %
	A1 = (b1 --> a) --> c,
	A2 = (b2 --> a) --> c,
	A3 = (b3 --> a) --> c.
seq(andoli4x,   [ A1, A2, b1 --> b2 --> c, c --> a ]  ==> up(a) ) :- %
	A1 = (b1 --> a) --> c,
	A2 = (b2 --> a) --> c.


seq(andoli4y,   [ A1,  b1 -->  c, c --> a ]  ==> up(a) ) :- %
	A1 = (b1 --> a) --> c.


seq(andoli4,   [ (a --> b) & ((a --> b)-->a)   ]  ==> up(b) ). %


seq(and0, [] ==> up(A)) :-
	 A = ( p  &  (p --> q) ) --> q.

seq(and1, [] ==> up(A)) :-  % unpr
	 A = ((a --> b) & ((a --> b)-->a))  --> c.

seq(and2, [] ==> up(A)) :-  % unpr.
        A = B --> C,
	B = ((x & y ) &(a --> (b-->c))) & d  ,
        C = c.
%%%  IMPLICATION, BOT , AND, OR %%%%%

seq(or1,  [a v b ] ==> up(A)) :-
	A = b v a.

seq(or2,  [a v b ] ==> up(c)).

seq(orax1,  [] ==> up(A)) :-
	A = a --> a v b.

seq(orax2,  [] ==> up(A)) :-
	A = b --> a v b.

seq(orax3,  [] ==> up(A)) :-
	A = ((a-->c)  & (b --> c)) --> (a v b -->c).


seq(orax4,  [] ==> up(A)) :-
	A = (a v b --> c ) --> a v ( b -->c).

% OTHER TESTS
%%      orax3
seq(ex1,  [] ==> up(A)) :-
	A = ((a-->c)  & (b --> c)) --> ( (d &(b v a))   -->c).

seq(ex2,  [] ==> up(A)) :-
	A = ((a-->c)  & (b --> c)) --> (a v b   -->c).




seq(sq1, [] ==>up( a & ( (s & r) v q) -->  a & (p v q)  ) ). % unprovable



seq(intp2,  []  ==>up(A) ):-
    A =  (p & q ) v (~p & q) v (p & ~q) v (~p & ~q).

seq(nintp2,  []  ==>up(A) ):-
    A =  (p & q ) v (~p & q) v (p & ~q) v (~p & ~a).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%    EXAMPLES



seq(tertium,  []  ==>up(p  v  ~ p) ).
seq(dummett,  []  ==>up( (a-->b) v (b-->a)) ).

%seq(exoli216,   [ (c --> a) &  ((c --> a) --> c) ]  ==>  up(a) ).
%
seq(exoli216,   [ ]  ==>  up(F) ) :-
	A = (p --> q) &  ((p --> q) --> p),
	F = A --> q.


seq(exoli217a,   [ A1 & A2 & (b_1 --> b_2  --> c) & (c --> a )]  ==> up(a) ) :-
	A1 = (b_1 --> a) --> c,
	A2 = (b_2 --> a) --> c.




seq(exoli217b,   [ A1 & A2 & A3 & (b_1 --> b_2 --> b_3 --> c) & (c --> a) ]  ==> up(a) ) :-
	A1 = (b_1 --> a) --> c,
	A2 = (b_2 --> a) --> c,
	A3 = (b_3 --> a) --> c.


seq(exoli236,   [ ((p --> q) --> p) & (p --> r) ]  ==> up(r) ).


seq(axiomOr1,  [] ==> up(p v (p --> q) )).
seq(axiomOr2,  [] ==> up(A)) :-
	A = (a v b --> c ) --> a v ( b -->c).



seq(exref1, [] ==>up( a & ( (s & r) v q) -->  a & (p v q)  ) ). % unprovable

seq(exref2,   [ A1 & A2 & (b_1 --> b_2  --> c) & (c --> a )]  ==> up(a) ) :-
	A1 = (b_1 --> a) --> c,
	A2 = (b_2 --> a) --> d.

seq(exref3,   [ A1  & (p_1 & q1  -->  p_2 )]  ==> up(p_2) ) :-
	A1 = (p_1  --> p_2) --> p_3.


%%   PAPER EXAMPLE

seq(exrefa,   [ ]  ==> up( A & B -->  p_2 v ~ p_4) ) :-
	A = (p_1  --> p_2) --> p_3,
	B =  p_1 &  p_4 --> p_2.

seq(exrefb,   []  ==> up(A & B -->  ~ p_4 v  p_2) ) :-
	A = (p_1  --> p_2) --> p_3,
	B =  p_1 &  p_4 --> p_2.

seq(exrefc,   []  ==> up(A & B -->  p_2 v  ~ p_4) ) :-
	A = (p_1  --> p_2) --> p_3,
	B =  p_1 &  p_4 --> (p_5 --> p_2).



seq(intp2,  []  ==>up(A) ):-
    A =  (p & q ) v (~p & q) v (p & ~q) v (~p & ~q).

seq(deMorgan1,   [ ]  ==> up( A ) ) :-
  A = ~( p & q) --> ~p v ~q.

seq(exOr1,  [ (c --> a v b)  ,  q v r  ]  ==>up( r v q) ).
seq(exOr2,  [ (c --> a v b)  ,  q v r  ]  ==>up( r ) ).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*

seq(tt1,   []  ==> up( S ) ) :-
	A = (a_1 <--> a_2) <--> a_3,
	S = A & a_3 --> ( a_2 <--> a_1).

seq(tt0,   []  ==> up( S ) ) :-
	%A = (a_1 <--> a_2) <--> a_3,
	%A = (a_1 <--> a_2),
	A = ( q --> p) & ( p --> q) ,
	S = A  -->  A.

seq(z0,   []  ==> up( S ) ) :-
	S = a & b  -->  b & a.
seq(z1,   []  ==> up( S ) ) :-
	S = #f  -->   a.

seq(tt2,   []  ==> up( S ) ) :-
	A = (a_1 <--> a_2) <--> a_3,
	S = A & a_3 --> ( a_1 <--> a_2).


seq(tt3,   []  ==> up( S ) ) :-
	A = (a_1 <--> a_2) <--> a_3,
	S = A --> ( a_3 --> ( a_1 <--> a_2)).


seq(tt4n,   []  ==> up( S ) ) :-
	A = (a_1 <--> a_2) <--> a_3,
	%B =  a_3 <--> (a_2 <--> a_1),
	%S = ( ((a_1 <--> a_2) --> a_3)  & ( a_2 <--> a_1 ) ) -->   a_3 .
	% S = (  A  & ( a_1 <--> a_2 ) ) -->   a_3 .
	S = (  A  & ( a_2 <--> a_1 ) ) -->   a_3 .
seq(tt4y,   []  ==> up( S ) ) :-
	A = (a_1 <--> a_2) <--> a_3,
	%B =  a_3 <--> (a_2 <--> a_1),
	%S = ( ((a_1 <--> a_2) --> a_3)  & ( a_2 <--> a_1 ) ) -->   a_3 .
	% S = (  A  & ( a_1 <--> a_2 ) ) -->   a_3 .
	S = (  A  & ( a_1 <--> a_2 ) ) -->   a_3 .




seq(ttx,   []  ==> up( S ) ) :-

	S = ( a <--> b)  <-->  ( b <--> a).

*/





%%%%%   SYJ   %%%%%%%%
%
%

% SYJ201+1.001
seq(syj201,   []  ==> up(S ) ) :-
  S = ( ( E12 --> C3 ) &   (E23 --> C3 ) & ( E31 --> C3)) --> C3,
  C3 =  p_1 & p_2 & p_3,
  E12 = p_1 <--> p_2,
  E23 = p_2 <--> p_3,
  E31 = p_3 <--> p_1.


% SYJ201+1.002
seq(syj201002,   []  ==> up(S ) ) :-
  S = ( ( E12 --> C5 ) &   (E23 --> C5 ) & ( E34 --> C5) & ( E45 --> C5)  & ( E51 --> C5) ) --> C5,
  C5 =  p_1 & p_2 & p_3 & p_4 & p_5,
  E12 = p_1 <--> p_2,
  E23 = p_2 <--> p_3,
  E34 = p_3 <--> p_4,
  E45 = p_4 <--> p_5,
  E51 = p_5 <--> p_1.

% SYJ201+1.003
seq(syj201003,   []  ==> up(S ) )  :-
  S = ( ( E12 --> C7 ) &   (E23 --> C7 ) & ( E34 --> C7) & ( E45 --> C7)  & ( E56 --> C7) &  ( E67 --> C7) &   (E71 --> C7)) --> C7,
  C7 =  p_1 & p_2 & p_3 & p_4 & p_5 & p_6 & p_7,
  E12 = p_1 <--> p_2,
  E23 = p_2 <--> p_3,
  E34 = p_3 <--> p_4,
  E45 = p_4 <--> p_5,
  E56 = p_5 <--> p_6,
  E67 = p_6 <--> p_7,
  E71 = p_7 <--> p_1.






% SYJ202+1.002
seq(syj202,   []  ==> up( S ) ) :-
	S = A --> B,
	A = (p11 v p12) &  (p21 v p22) & (p31 v p32),
	B = (p11 & p21) v  (p11 & p31) v (p21 & p31) v (p12 & p22) v  (p12 & p32) v (p22 & p32).

%SYJ203+1.002
seq(syj203,   []  ==> up( S ) ) :-
      S = (A2 --> f) --> f,
      A2 = (p_1 & p_2) v (p_1 --> f) v (p_2 --> f).

% SYJ204+1.002
seq(syj204,   []  ==> up( S ) ) :-
      S = A --> p_0,
      A = p_2 &  ( p_1 --> ( p_1 --> p_0)) &  ( p_2 --> ( p_2 --> p_1)).

% SYJ205+1.001
seq(syj205,   []  ==> up( S ) ) :-
   S = ( C --> f) & (D --> f),
   A1 =  a_0 --> f,
   A2 =	(b_1--> b_0) --> a_1,
   A3 = (b_0 --> a_1 ) --> a_0,
   C = A1 & A2 & A3,
   D = A3 & A2 & A1.

% SYJ206+1.002
seq(syj206,   []  ==> up( S ) ) :-
   S = (a_1 <--> a_2) <--> (a_2 <--> a_1).


seq(syj206003,   []  ==> up( S ) ) :-
   %S = ((a_1 <--> a_2) <--> a3 ) <-->  ( a_3 <--> (a_2 <--> a_1) ).
	A =   (a_1 <--> a_2) <--> a_3,
	B =  a_3 <--> (a_2 <--> a_1),
	S =  A <--> B.







all([tertium,dummett,exoli216,exoli217a, exoli217b, exoli236,axiomOr1, axiomOr2,
exref1,deMorgan1, exOr1, exOr2]).

alltest :-
	all(LabList),
	forall( member(Lab,LabList), testlabpdf(Lab,_)).






