
\section{Proof-search}\label{sec:search}

Here we formalize the proof-search procedure outlined in
Sect.~\ref{sec:NCR}. Proof-search is performed by the mutually
recursive functions \UpSearch (Fig.~\ref{fun:UpSearch}) and \DownExp
(Fig.~\ref{fun:DownExp}).  In detail:

\begin{enumerate}[label=(P\arabic*),ref=(P\arabic*)]
\item\label{def:UpSearch} Given $\s=\sequp{\G}{C}{\D}$,
  \UpSearch{$\s$} returns either an $\NCR$-derivation or an
  $\RNCR$-derivation of $\s$.  We call $\s$ the \emph{main parameter}
  of \UpSearch{$\s$}.

\item\label{def:DownExp} Let $\Dcal$ be an $\NCR$-derivation of
  $\s=\seqdown{\Gamma}{H}{C}{\Delta}{\Theta}$ such that
  $H\not\in\Gamma$, $C\in\posSf{H}$ and $\Theta \subseteq\posSfm{H}$,
  and let $K\in\posSf{C}\cap\Disj$.  Then, \DownExp{$\Dcal$,$K$}
  returns one of the following values:

  \begin{enumerate}
  \item\label{def:DownExp:1} an $\NCR$-derivation of
    $\seqdown{\Gamma}{H}{K}{\Delta}{\Theta'}$, with
    $\Theta\subseteq\Theta' \subseteq\posSfm{H}$;

  
\item \label{def:DownExp:2} a pair $\stru{\rho,\Ecal}$, where 
$\rho$ is a reduction  $(\redto{H}{A\to B}{\Theta'})$ such that
$\Theta\subseteq\Theta' \subseteq\posSfm{H}$, and $\Ecal$ is an $\RNCR$-derivation of
     $\sequp{\Gamma,\Theta'}{A}{\Delta}$.


  % \item \label{def:DownExp:2} a pair $\stru{\,(\redto{H}{A\to
  %       B}{\Theta'})\,,\,\Ecal\,}$, where $\Theta\subseteq\Theta'
  %   \subseteq\posSfm{H}$ and $\Ecal$ is an $\RNCR$-derivation of
  %   $\sequp{\Gamma,\Theta'}{A}{\Delta}$.

\end{enumerate}
We call the root sequent $\s$ of $\Dcal$ the \emph{main parameter} of
\DownExp{$\Dcal$,$K$}.
\end{enumerate}
% 
Intuitively, \UpSearch{$\s$} simulates a step of $\up$-expansion of
$\s$, \DownExp{$\Dcal$, $K$} performs a $\down$-expansion step of the
root sequent $\s$ of $\Dcal$ with the goal to extract $K$.  In both
functions, the choice of the step to be executed is determined by the
right formula $C$ of $\s$.  Except for the case at
line~\ref{UpSearch:IRR} of \UpSearch and the case at
line~\ref{DownExp:ID} of \DownExp that directly return a derivation,
every case of \UpSearch and \DownExp corresponds to the application of
a rule of the calculus $\NCR$. One can easily check that the displayed
cases are exhaustive. Some cases overlap (e.g., the cases concerning
$C\in\Prime$ of \UpSearch); if this happens, the procedure
non-deterministically chooses one of the enabled cases.

In \UpSearch, if $C\in\Prime$ and the conditions corresponding to the
cases of rules $\coerc$, $\botint$, $\lor E$, $\ruleRP$, $\ruleRC$ do
not hold, then $\s$ is irreducible.  The crucial points are that it is
irrelevant which of the enabled cases is selected and no backtrack is
needed (each case returns a result).  In Figs.~\ref{fun:UpSearch}
and~\ref{fun:DownExp}, we use the following auxiliary functions:  
\begin{itemize}
\item Let $\Cbo\in\{\NCR,\RNCR\}$. Given a sequent $\sigma$, a
  (possibly empty) set of $\Cbo$-derivations $\Pcal$ and a rule
  $\Rcal$ of $\Cbo$, \Build{$\Cbo$,$\s$,$\Pcal$,$\Rcal$} constructs
  the $\Cbo$-derivation $\Dcal$ having root sequent $\s$, root rule
  $\Rcal$ and the derivations in $\Pcal$ as immediate subderivations
  of $\Dcal$.  \BuildId{$\s$} abbreviates
  \Build{$\NCR$,$\s$,$\emptyset$, $\ruleID$}.

\item Let $\sigma$ be a sequent, $\Dcal$ an $\RNCR$-derivation with
  root a $\down$-sequent $\sigdown$, $\rho$ a reduction for $\sigdown$
  and $\Rcal\in\{\lor E_k,\to\! E\}$.
  \Build{$\RNCR$,$\sigma$,$\stru{\rho,\Dcal}$,$\Rcal$} constructs the
  $\RNCR$-derivation of $\sigma$ obtained by applying $\Rcal$ with
  premises $\rho$ and $\sigdown$.
\end{itemize}
We assume that, whenever \Build and \BuildId are called, their
arguments are correctly instantiated, so that  well-defined
$\Cbo$-derivations are built.

% Given a sequent $\sigma$, a (possibly empty) set of
% $\Cbo$-derivations $\Ical$ and a rule $\Rcal$ of $\Cbo$,
% \Build{$\Cbo$,$\s$,$\Ical$,$\Rcal$} constructs the $\Cbo$-derivation
% $\Dcal$ having root sequent $\s$, root rule $\Rcal$ and the
% derivations in $\Ical$ as immediate subderivations of $\Dcal$;
% \BuildId{$\s$} abbreviates \Build{$\NCR$,$\s$,$\emptyset$, $\ruleID$}.
% Let $\sigdown$ be a $\down$-sequent and $\rho$ a reduction for $\s$.
% If $\sigdown$ is the root sequent of an $\RNCR$-derivation $\Dcal$, by
% \Build{$\RNCR$,$\sigma$,$\stru{\rho,\Dcal}$,$\Rcal$} we denote the
% $\RNCR$-derivation of $\sigma$ obtained by applying $\Rcal$ to
% $\sigdown$ using the reduction $\rho$ ($\Rcal =\lor E$ or $\Rcal =\to
% E$).

To prove the \emph{correctness} of the proof-search procedure, we must
show that points~\ref{def:UpSearch} and~\ref{def:DownExp} hold.  To
this aim, we introduce the order relation $\prec$ between sequents.
The \emph{size of $A$}, denoted with $\size{A}$, is the number of
logical connectives occurring in $A$ (for $F\in\Prime$, $\size{F}=0$).
Let {\small
  \[
  \Set{\sequp{\G}{A}{\D}}\;=\;\G\cup\{A\}\cup\D
  \qquad
  \Set{\seqdown{\G}{H}{A}{\D}{\Theta}}\;=\;\G\cup\{A\}\cup\D\cup\Theta
  \]}Let $\s_1$ and $\s_2$ be sequents; $\s_1\prec \s_2$ iff one of
the following conditions holds:

\begin{enumerate}[label=(\arabic*),ref=(\arabic*)]
\item\label{prec:1} $\Set{\s_1}=\Lambda\cup\Xi$,
  $\Set{\s_2}=\Lambda\cup\{Y\}$ and, for every $X\in\Xi$, $\size{X} <
  \size{Y}$;

\item\label{prec:2} $\size{\Set{\s_1}}=\size{\Set{\s_2}}$ and
  $\size{\G_1} < \size{\G_2}$ ($\G_i$ is the context of $\s_i$);

\item\label{prec:3} $\size{\Set{\s_1}}=\size{\Set{\s_2}}$, $\G_1 =
  \G_2$ and $\size{\D_1} < \size{\D_2}$ ($\D_i$ is the restart set of
  $\s_i$).

\end{enumerate}
One can check that $\prec$ is well-founded.  We can prove
points~\ref{def:UpSearch} and~\ref{def:DownExp} by induction on
$\prec$.  Let $f$, $f'$ be any of the functions \UpSearch and \DownExp
and let $f(\s)$ denote an invocation of $f$ with main parameter $\s$.
One can check that, whenever in the execution of $f(\s)$ we call
$f'(\s')$, then $\s'\prec \s$ and the preconditions stated
in~\ref{def:DownExp} hold; hence, by induction hypothesis, the
returned value is correct.  For instance, let us consider the case
treating the rule $\coerc$ of Fig.~\ref{fun:UpSearch}
(lines~\ref{UpSearch:coerc:case}--\ref{UpSearch:coercRetRef}).  The
call to \DownExp at line~\ref{UpSearch:coerc} has main parameter $\s'=
\seqdown{\G_H}{H}{H}{C,\D}{}$.  Since $\size{\s'}=\size{\s}$ (indeed,
$\size{C}=0$) and $\size{\G_H}< \size{\G}$, we have $\s'\prec \s$.  By
induction hypothesis, the value $\pi$ returned by \DownExp satisfies
one between~\ref{def:DownExp:1} and~\ref{def:DownExp:2}.  In the
former case, an $\NCR$-derivation of $\s$ with root rule $\coerc$ is
returned at line~\ref{UpSearch:coercRetProof}; in the latter case,
$\pi$ is a pair $\stru{(\redto{H}{A\to B}{\Theta'}),\Ecal}$, and an
$\RNCR$-derivation of $\s$ with root rule $\to E$ is returned at
line~\ref{UpSearch:coercRetRef}.  The proof of the other cases is
similar.  In the recursive call at line~\ref{UpSearch:orE0} (case
$\lor E$) the main parameter is $\s'=\sequp{A,\G_H,\Theta}{C}{\D}$; we
have $A\in \posSfm{H}$ and, by the induction
hypothesis~\ref{def:DownExp:1} on the recursive call at
line~\ref{UpSearch:orEpi}, $\Theta\subseteq \posSfm{H}$; this implies
$\s'\prec \s$.  In the recursive call at line~\ref{UpSearch:restc}
(case $\ruleRC$) the main parameter is $\s'=\sequp{\G}{D}{C,\D_D}$;
here $\Set{\s}=\Set{\s'}$ and $\s'\prec \s$ follows from
$\size{C,\D_D}< \size{\D}$.  The condition in case $\land E_k$ of
\DownExp (line~\ref{DownExp:andEkcond}) is needed to guarantee that
the invariant~\ref{inv:down} holds.  By inspecting all the cases, we
get:

\begin{proposition}
  The functions \UpSearch and \DownExp are correct.
\end{proposition}


%% custom switch
\SetKwSwitch{Switch}{Case}{Other}{non-deterministically
  choose}{}{case}{otherwise}{endcase}{}

\newcommand{\lab}[1]{(#1)}

% #####################################################################
%%% FUNCTION UP SEARCH
\begin{function}[h]
\DontPrintSemicolon
%===================================================================
\Fn{\UpSearch{$\,\sigma\,=\,\sequp{\Gamma}{C}{\Delta}\,$}}{
%===============================================================
% \Output{A $\NCR$-derivation of $\sigma$ or a $\RNCR$-derivation   of $\sigma$} 
%====================================================================
%% CASES
\Switch{}{\label{UpSearch:switch}
%-----------------------------------------------------
%% CASE coerc
\uCase(\tcp*[f]{$\coerc$})
{$C\in\PV$  and there is $H\in\Gamma$ such that $C\in\posSf{H}$\label{UpSearch:coerc:case}}
{
\tcp{$\stru{H,C,\coerc}$ is a closing match for $\s$}
$\pi \ass $ 
\DownExp{\,\BuildId{\,$\seqdown{\G_H}{H}{H}{C,\D}{}$\,}\,,\,$C$\,}\label{UpSearch:coerc}\;
\lIf{$\pi$ is an $\NCR$-der.}
{\Return  \Build{$\NCR$, $\sigma$, $\{\pi\}$, $\coerc$}\label{UpSearch:coercRetProof}}
\lElse(\tcp*[f]{\scriptsize $\pi=\stru{\,(\redto{H}{A\to B}{\Theta'})\,,\,\Ecal\ }$})
{\Return \Build{$\RNCR$, $\sigma$, $\pi$, $\to E$}\label{UpSearch:coercRetRef}} 
}%endcase coerc
%-----------------------------------------------------
%% CASE botE
\uCase(\tcp*[f]{$\botint$})
{$C\in\Prime$ and there is $H\in\Gamma$ such that $\bot\in\posSf{H}$}{
\tcp{$\stru{H,\bot,\botint}$ is a closing match for $\s$}
$\pi \ass $ 
\DownExp{\,\BuildId{\,$\seqdown{\G_H}{H}{H}{C,\D}{}$\,}\,,\,$\bot$\,}\label{UpSearch:botE}\;
\lIf{$\pi$ is an $\NCR$-der.}
{\Return \Build{$\NCR$, $\sigma$, $\{\pi\}$, $\botint$}\label{UpSearch:botERetProof}}
\lElse(\tcp*[f]{\scriptsize $\pi=\stru{\,(\redto{H}{A\to B}{\Theta'})\,,\,\Ecal\ }$})
{\Return\Build{$\RNCR$, $\sigma$, $\pi$, $\to E$} \label{UpSearch:botERetRef}}  
}%endcase botE
%-----------------------------------------------------
%% CASE orE
\uCase(\tcp*[f]{$\lor E$})
{$C\in\Prime$ and there is $H\in\Gamma$ such that there is  $A\lor B \in\posSf{H}$}{
\tcp{$\stru{H,A\lor B,\lor E}$ is a closing match for $\s$}
$\pi \ass $ 
\DownExp{\,\BuildId{\,$\seqdown{\G_H}{H}{H}{C,\D}{}$\,}\,,\,$A\lor B$\,}\label{UpSearch:orEpi}\;
\uIf{$\pi$ is an $\NCR$-derivation of $\seqdown{\G_H}{H}{A\lor B}{C,\D}{\Theta}$}{ 
% if ###
$\rho\ass$ $\redto{H}{A\lor B}{\Theta}$ \;
$\Dcal_0\ass$ \UpSearch{$\sequp{A,\G_H,\Theta}{C}{\D}$}\label{UpSearch:orE0}\;
\lIf{$\Dcal_0$ is an $\RNCR$-der.}
{\Return \Build{$\RNCR$, $\s$, $\stru{\rho, \Dcal_0}$,$\lor E_0$}\label{UpSearch:orERef0Ret}}
$\Dcal_1\ass$ \UpSearch{$\sequp{B,\G_H,\Theta}{C}{\D}$}\label{UpSearch:orE1}\;
\lIf{$\Dcal_1$ is an $\RNCR$-der.}
{\Return \Build{$\RNCR$, $\s$, $\stru{\rho,\Dcal_1}$,$\lor E_1$}\label{UpSearch:orE1RefRet}}
{\Return \Build{$\NCR$, $\s$, $\{\pi,\Dcal_0,\Dcal_1\}$,\,$\lor E$}\label{UpSearch:orEProofRet}}\;
}%endif ###
\lElse(\tcp*[f]{\scriptsize $\pi=\stru{\,(\redto{H}{A'\to B'}{\Theta'})\,,\,\Ecal\ }$})
{\Return \Build{$\RNCR$, $\sigma$, $\pi$, $\to E$}\label{UpSearch:orERetRef}} 
}%encase orE
%-----------------------------------------------------
%% CASE REST PRIME
\uCase(\tcp*[f]{$\ruleRP$})
{$C\in\Prime$ and  there is $p\in\Delta\cap \PV$ and $H\in\G$
such that   $p\in\posSf{H}$}{
\tcp{$\stru{H,p,\ruleRP}$ is a closing match for $\s$}
$\pi \ass $ 
\DownExp{\,\BuildId{\,$\seqdown{\G_H}{H}{H}{C,\D}{}$\,}\,,\,$p$\,}\label{UpSearch:restcp}\;
\lIf{$\pi$ is an $\NCR$-der.}
{\Return  \Build{$\NCR$, $\sigma$, $\{\pi\}$, $\ruleRP$}\label{UpSearch:restpRetProof}}
\lElse(\tcp*[f]{\scriptsize $\pi=\stru{\,(\redto{H}{A\to B}{\Theta'})\,,\,\Ecal\ }$})
{\Return \Build{$\RNCR$, $\sigma$, $\pi$, $\to E$}\label{UpSearch:restpRetRef}} 
}%encase   REST PRIME
%----------------------------------------------------------------
%% CASE  REST COMP
\uCase(\tcp*[f]{$\ruleRC$})
{$C\in\Prime$  and
there is $D\in\Delta$ such that $D\not\in\Prime$}{
$\Dcal_0\ass$ \UpSearch{$\sequp{\G}{D}{C,\D_D}$}\label{UpSearch:restc}\;
\lIf{$\Dcal_0$ is an $\NCR$-der.}
{\Return \Build{$\NCR$, $\sigma$, $\{\Dcal_0\}$, $\ruleRC$}\label{UpSearch:restcRetProof}}
\lElse{\Return  \Build{$\RNCR$, $\sigma$, $\{\Dcal_0\}$, $\ruleRC$ } \label{UpSearch:restcRetRef}}
}%encase REST COMP
%----------------------------------------------------------------
%% CASE IRR
\lCase(\tcp*[f]{$\ruleIRR$}){$C\in\Prime$ and $\s$ is irreducible}{
{\Return \Build{$\RNCR$,$\s$,$\emptyset$,$\ruleIRR$}}\label{UpSearch:IRR}
}%endcase IRR
%==========================================================
%% CASE andI
\uCase(\tcp*[f]{$\land I$}){$C= A_0 \land A_1$}{
$\Dcal_0 \ass $ \UpSearch{$\sequp{\Gamma}{A_0}{\D}$}\label{UpSearch:landI0}\;
\lIf{$\Dcal_0$ is an $\RNCR$-derivation}
{\Return  \Build{$\RNCR$,$\sigma$, $\{\Dcal_0\}$, $\land I_0$ }}\label{UpSearch:landI0Ret}
$\Dcal_1 \ass $ \UpSearch{$\sequp{\Gamma}{A_1}{\D}$}\label{UpSearch:landI1}\;
\lIf{$\Dcal_1$ is an $\RNCR$-derivation}
{\Return  \Build{$\RNCR$,$\sigma$, $\{\Dcal_1\}$, $\land I_1$ }}\label{UpSearch:landI1Ret}
\Return\Build{$\NCR$,$\sigma$, $\{\Dcal_0,\Dcal_1\}$, $\land I$}\label{UpSearch:landIRet}\; 
}%endcase andI
%-----------------------------------------------------
%% CASE orI
\uCase(\tcp*[f]{$\lor I$}){$C= A  \lor B$}{
$\Dcal_0 \ass $ \UpSearch{$\sequp{\Gamma}{A}{B,\Delta}$}\label{UpSearch:lorI0}\;
\lIf{$\Dcal_0$ is an  $\NCR$-derivation}
{\Return\Build{$\NCR$,$\sigma$,$\{\Dcal_0\}$, $\lor I$} }\label{UpSearch:lorIRetProof}
\lElse{\Return\Build{$\RNCR$,$\sigma$,$\{\Dcal_0\}$, $\lor I$}\label{UpSearch:lorIRetRef}}
}%endcase orI
%--------------------------------------------------
%% CASE implI
\uCase(\tcp*[f]{$\to I$}){$C= A \to B$}{
$\Dcal_0 \ass $ \UpSearch{$\sequp{A,\Gamma}{B}{\Delta}$}\label{UpSearch:imI}\;
\lIf{$\Dcal_0$ is an  $\NCR$-derivation}
{\Return\Build{$\NCR$,$\sigma$,$\{\Dcal_0\}$, $\to I$} }\label{UpSearch:toIRetProof}
\lElse{\Return\Build{$\RNCR$,$\sigma$,$\{\Dcal_0\}$, $\to I$}\label{UpSearch:toIRetRef}}
}%endcase implI
}%end switch
}%end Fn
\caption{UpSearch}\label{fun:UpSearch}
\end{function}
%#####################################################################
\FloatBarrier







\SetKwSwitch{Switch}{Case}{Other}{switch}{}{case}{otherwise}{endcase}{endsw}
%#####################################################################
%%% FUNCTION DOWN EXPAND
\begin{function}[t]%\mediumfont
\DontPrintSemicolon
%======================================================================
\Fn%(\tcp*[f]{$\Dcal$ is an $\NCR$-derivation of  $\s=\seqdown{\Gamma}{H}{C}{\Delta}{\Theta}$ })
{\DownExp{\,$\Dcal$,\,$K$\,}}{
% %======================================================================
 \Input{$\Dcal$ is an $\NCR$-derivation of  $\s=\seqdown{\Gamma}{H}{C}{\Delta}{\Theta}$
as in~\ref{def:DownExp}}
%%% CASES
\Switch{$C$}{\label{DownExp:switch}
%% PRIME, DISJ
\lCase(\tcp*[f]{here $C=K$}){$C\in\Prime$ or $C=C_0\lor C_1$}{
{\Return $\Dcal$}\label{DownExp:ID}
} %endcase 
%% AND  andkE
\uCase(\tcp*[f]{$\land E_k$})
{$C=A_0\land A_1$ and $K\in\posSf{A_k}$, with $k\in\{0,1\}$\label{DownExp:andEkcond}}{
 $\Ecal \ass$  
\Build{$\NCR$,$\seqdown{\Gamma}{H}{A_k}{\Delta}{A_{1-k},\Theta}$,\,$\{\Dcal\}$,\,$\land E_k$}\label{DownExp:andEk}\;
\Return{\DownExp{$\Ecal$, $K$}}\label{DownExp:andk0Ret}\;
}%endcase and Ek
%% CASE implE
\uCase(\tcp*[f]{$\to E$}){$C=A\to B$}{
$\Ecal\ass$ \UpSearch{$\sequp{\Gamma,\Theta}{A}{\Delta}$}\label{DownExp:impl}\;
\uIf{$\Ecal$ is an $\NCR$-derivation}{ 
  $\Dcal'\ass $  \Build{$\NCR$, $\seqdown{\G}{H}{B}{\D}{\Theta}$, $\{\Dcal,\Ecal\}$,$\to E$}\;
 \Return{\DownExp{$\Dcal'$, $K$}}}
\lElse{\Return{$\stru{\,(\redto{H}{A\to B}{\Theta})\,, \,\Ecal\,}$   }}
}%endcase IMPL
}%endswitch
}%end Fn
\caption{DownExp}\label{fun:DownExp}
\end{function}
%#####################################################################





To conclude, by~\ref{def:UpSearch} and the soundness of $\RNCR$
(Prop.~\ref{prop:soundRNCR}), if $\s$ is valid then \UpSearch{$\s$}
returns an $\NCR$ derivation of $\s$. Hence:

\begin{proposition}[Completeness of $\NCR$]\label{prop:complNCR}
  If $\s$ is valid then $\NCR\der \s$.
\end{proposition}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "clnat"
%%% End:
