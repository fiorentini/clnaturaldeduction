

\section{The refutation calculus $\RNCR$}\label{sec:ref}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  REFUTATION CALCULUS RNCR 

\begin{figure}[t]\small
  \[
  \begin{array}{c}
    %% Ref NCR Irr
    \AXC{}
    \RightLabel{$\ruleIRR$}
\UIC{$\s$}
   % \UIC{$\sequp{\G}{F}{\Delta}{}$}
    \DP
\quad\mbox{$\s$ irreducible}
  \hspace{3em}
%%% Ref NCR Restart COMP
     \AXC{$\sequp{\G}{D}{F,\D_D}$}
     \RightLabel{$\ruleRC$}
     \UIC{$\sequp{\G}{F}{D,\D}$}
 \DP
\quad D\not\in\Prime,\, F\in\Prime
\\[4ex]
%%% RNCR AND I
     \AXC{$\sequp{\G}{A_k}{\D}$}
     \RightLabel{$\land I_k$}
     \UIC{$\sequp{\G}{A_0\land A_1}{\D}$}
 \DP
%\quad k\in\{0,1\} 
 \hspace{2em}
%%% RNCR OR I
     \AXC{$\sequp{\G}{A}{B,\D}$}
     \RightLabel{$\lor I$}
     \UIC{$\sequp{\G}{A\lor B}{\D}$}
 \DP
\hspace{2em}
%%% RNCR IMPL I
     \AXC{$\sequp{A,\G}{B}{\D}$}
     \RightLabel{$\to I$}
     \UIC{$\sequp{\G}{A \to B}{\D}$}
 \DP
\\[4ex]
\begin{array}{cc}
%%% RNCR OR E_k
\AXC{$\redto{H}{A_0\lor A_1}{\Theta}$}
\AXC{$\sequp{A_k,\G_H,\Theta}{F}{\D}$}
\RightLabel{$\lor E_k$}
\BIC{$\sequp{H,\G}{F}{\D}$}
 \DP
&\quad F\in\Prime,\,k\in\{0,1\}
 \\[4ex]
% %%% RNCR  IMPL  E
 \AXC{$\redto{H}{A\to  B}{\Theta}$}
 \AXC{$\sequp{\G_H,\Theta}{A}{F,\D}$}
 \RightLabel{$\to E$}
 \BIC{$\sequp{H,\G}{F}{\D}$}
  \DP
&\quad  F\in\Prime
\end{array}
% \\[4ex]
%     % %%%%% WHERE ...
%      \begin{minipage}{40em}
%  $F\in\Prime$,   $k\in\{0,1\}$ 
%  \end{minipage}
   \end{array}
   \]
  \caption{The  refutation calculus $\RNCR$.}
  \label{fig:RNCR}
\end{figure}        

To prove the completeness of $\NCR$ and the correctness of the
proof-search strategy defined in Sect.~\ref{sec:search}, we introduce
the calculus $\RNCR$ for classical unprovability. This calculus is
dual to $\NCR$ in the following sense: the proof-search procedure of
Sect.~\ref{sec:search} returns an $\RNCR$-derivation of $\s$ whenever
it fails to build an $\NCR$-derivation of $\sigma$.  Since sequents
provable in $\RNCR$ are not valid (see Prop.~\ref{prop:soundRNCR}
below), we get the completeness of $\NCR$; indeed, if $\s$ is valid,
the proof-search procedure must return an $\NCR$-derivation of
$\sigma$.

Let the \emph{reduction relation} $\redtoname$ be the smallest
relation defined by the rules:
\[
%%% REDUCTION ID
\AXC{}
\UIC{$\redto{H}{H}{}$}
\DP
\qquad
%%% REDUCTION AND
\AXC{$\redto{H}{A_0\land A_1}{\Theta}$}
\UIC{$\redto{H}{A_k}{A_{1-k},\Theta}$}
\DP
\;k\in\{0,1\}
\qquad
%%% REDUCTION IMPL
\AXC{$\redto{H}{A \to B}{\Theta}$}
\UIC{$\redto{H}{B}{\Theta}$}
\DP
\]
where $A$, $B$, $H$ are formulas, and $\Theta$ is a finite, possibly
empty, set of formulas. The following properties of $\redtoname$ can
be easily proved:

\begin{lemma}\label{lemma:reduces}
 \mbox{}
  \begin{enumerate}[label=(\roman*),ref=(\roman*)]
  \item\label{lemma:reduces:1} If $\redto{H}{K}{\Theta}$, then the
    formula $(K\land \Theta^\land) \to H$ is  valid.

  \item\label{lemma:reduces:2}
    $\NCR\der\seqdown{\G}{H}{K}{\D}{\Theta}$ implies
    $\redto{H}{K}{\Theta}$.
  \end{enumerate}
\end{lemma}

\begin{proof}
   Point~\ref{lemma:reduces:1} follows by induction on the depth of the
   derivation of $\redto{H}{K}{\Theta}$.  Point~\ref{lemma:reduces:2}
   can be proved by induction on the depth of the $\NCR$-derivation of
   $\seqdown{\G}{H}{K}{\D}{\Theta}$.\qed
\end{proof}

\noindent
An $\up$-sequent $\sigirr=\sequp{\Gamma}{F}{\Delta}$ is
\emph{irreducible} iff the following conditions hold:

\begin{enumerate}[label=($\mathrm{Irr}\arabic*$),ref=($\mathrm{Irr}\arabic*$)]
\item\label{irr1} $(\{F\}\cup\Delta )\;\subseteq\; \Prime$;
 
\item\label{irr2} $(\{\bot,F\}\cup\Delta)\,\cap\, \posSf{\Gamma} =\emptyset$
  and  $A\lor B\not\in\posSf{\Gamma}$, for every $A\lor B\in\Lcal$.
\end{enumerate}


\noindent
In proof-search,
when in an $\up$-expansion phase we get an irreducible sequent
$\sigirr$, the phase ends. As a matter of fact, by
condition~\ref{irr1} no rule of $\rulesup$ can be applied to
$\sigirr$;  by
condition~\ref{irr2},
$\sigirr$ does not admit any closing match.
Irreducible sequents are not valid. Indeed, let
$\Intp{\sigirr} =\posSf{\Gamma}\cap\PV$ be the \emph{interpretation}
associated with $\sigirr$.  We can prove that:

\begin{lemma}\label{lemma:irr}
  Let $\sigirr$ be an irreducible sequent.  Then,
  $\Intp{\sigirr}\not\models \Fm{\sigirr}$.
\end{lemma}

\begin{proof}
  Let $\sigirr=\sequp{\Gamma}{F}{\Delta}$.  By induction on $A$, we
  can show that $A\in\posSf{\Gamma}$ implies $\Intp{\sigirr}\models
  A$; thus $\Intp{\sigirr}\models\G^\land$. 
Moreover,  by~\ref{irr1} and~\ref{irr2}
%  definition of irreducible sequent 
% every formula in $\{F\}\cup\Delta$
%   is a prime formula not occurring in $\posSf{\Gamma}$,
we get  $\Intp{\sigirr}\not\models F\lor\D^\lor$. Hence
  $\Intp{\sigirr}\not\models \Fm{\sigirr}$.  \qed
\end{proof}

Rules of the calculus $\RNCR$ are shown in Fig.~\ref{fig:RNCR}.  Rules
$\lor E_k$ and $\to\! E$ have, as premises, a reduction $\rho$ (side
condition) and an $\up$-sequent $\s$; we say that $\rho$ is a
\emph{reduction for} $\s$.
% Note that rules $\lor E_k$ and $\to E$ have a reduction as premise
% (side condition).  We say that:
% \begin{itemize}
% \item[-] $\redto{H}{A_0\lor A_1}{\Theta}$ is a reduction for
%   $\sequp{A_k,\G_H,\Theta}{F}{\D}$ ($k\in\{0,1\}$);
% \item[-] $\redto{H}{A\to B}{\Theta}$ is a reduction for
%   $\sequp{\G_H,\Theta}{A}{F,\D}$.
% \end{itemize}
% \noindent
% Note that, by Lemma~\ref{lemma:reduces}\ref{lemma:reduces:1},
% if $\redto{H}{K}{\Theta}$ is a reduction for $\s$ and
% $\Ical\not\models\Fm{\s}$, then  $\Ical\models H$.
% By~Lemma~\ref{lemma:reduces}\ref{lemma:reduces:1} we have:
% \begin{lemma}\label{lemma:reductionForSgma}
%   Let $\redto{H}{K}{\Theta}$ be a reduction for $\s$ and $\Ical$ a
%   classical interpretation.  If $\Ical\not\models\Fm{\s}$, then
%   $\Ical\models H$.
% \end{lemma}
%
An $\RNCR$-derivation $\Dcal$ consists of a single branch whose
top-most sequent is irreducible; we call it the
\emph{irreducible sequent of $\Dcal$}.  
%Using Lemma~\ref{lemma:irr},
%and~\ref{lemma:reductionForSgma}, 
%we get:

\begin{lemma}\label{lemma:RNCRsound}
  Let $\Dcal$ be an $\RNCR$-derivation and let $\sigirr$ be the
  irreducible sequent of $\Dcal$.  For every $\s$ occurring in
  $\Dcal$, $\Intp{\sigirr}\not\models\Fm{\s}$.
\end{lemma}

% \begin{proof}
% By induction on the depth of $\Dcal$. %To treat rules $\lor E_k$ and $\to\! E$,
% Note  that, by Lemma~\ref{lemma:reduces}\ref{lemma:reduces:1},
% if $\redto{H}{K}{\Theta}$ is a reduction for $\s$ and
% $\Ical\not\models\Fm{\s}$, then  $\Ical\models H$.\qed 
% \end{proof}

\begin{proof}
  By induction on the depth $d$ of $\s$.  If $d=0$, then $\s=\sigirr$
  and the assertion follows by Lemma~\ref{lemma:irr}.  Let $\s$ be the
  conclusion of an application of one of the rules $\lor E_k$, $\to\!
  E$ with side condition $\rho$ and premise $\s'$.  By induction
  hypothesis $\Intp{\sigirr}\not\models\Fm{\s'}$; by
  Lemma~\ref{lemma:reduces}\ref{lemma:reduces:1}, we get
  $\Intp{\sigirr}\not\models\Fm{\s}$.  The other cases easily follow.
  \qed
\end{proof}

Accordingly, from an $\RNCR$-derivation of $\s$ we can extract an
interpretation falsifying $\s$, namely the interpretation  $\Intp{\sigirr}$.
This implies that:


\begin{proposition}[Soundness of $\RNCR$]\label{prop:soundRNCR}
  If $\RNCR\der \s$ then $\s$ is not valid.
\end{proposition}

% , given an $\RNCR$-derivation of $\s$, the interpretation
% associated with its irreducible sequent witnesses the non-validity of
% $\sigma$.


\begin{example}
  Let us consider the following $\RNCR$-derivation $\Dcal$: 
  %where
  %$A=(p_1 \to p_2) \to p_3$ and $B= p_1 \land p_4 \to p_2$:
  \[\small
   \begin{array}{c}
    A \,=\, (p_1 \to p_2) \to p_3
     \qquad
     B\,=\, p_1 \land  p_4 \to p_2
     \\[.5ex]
    \infer[\to I]{
      \labsequp{}{A \land B\to p_2\lor \neg p_4}{}{0}}{
      \infer[\lor I]{
        \labsequp{A\land B}{p_2\lor \neg p_4}{}{1}}{
        \infer[\to E]{
          \labsequp{A \land B}{p_2}{\neg p_4}{2}}{
          \redto{A \land B}{B}{A} & \infer[\land I_0]{
            \labsequp{A}{p_1 \land  p_4}{p_2,\,\neg p_4}{3}}{
            \infer[\ruleRC]{
              \labsequp{A}{p_1}{p_2,\neg p_4}{4}}{
              \infer[\to I]{
                \labsequp{A}{\neg p_4}{p_1,p_2}{5}}{
                \infer[\ruleIRR]{
                  \labsequp{p_4,\,A}{\bot}{p_1,p_2}{6}}
                {}
              }
            }
          }
        }
      }
    }
  \end{array}
  \]
  The construction of $\Dcal$ follows by the failure of the
  proof-search for $\s_0$.  Indeed, $\up$-expansion of $\s_0$ yields
  $\s_2$.  Here we can non-deterministically choose either to continue
  the $\up$-expansion phase by applying $\ruleRC$ (restart from $\neg
  p_4$) or to start a $\down$-expansion phase from $\sigdown_0
  =\seqdown{}{A\land B}{A\land B}{\D}{}$ with $\D=\{p_2,\neg p_4 \}$
  (indeed, $\stru{A\land B, p_2,\coerc}$ is a closing match for
  $\s_2$).  We follow the latter way.  By~\ref{inv:down}, we apply
  $\land E_1$ and we get $\sigdown_1 =\seqdown{}{A\land B}{B}{\D}{A}$.
  By applying $\to E$, we get the conclusion $\sigdown_2
  =\seqdown{}{A\land B}{p_2}{\D}{A}$ and the premise $\s_3$, which
  must be $\up$-expanded.  The $\down$-expansion phase ends, since we
  can apply $\coerc$ with premise $\sigdown_2$ and conclusion $\s_2$.
  Note that we have built an $\NCR$-derivation of $\sigdown_1$ hence,
  by Lemma~\ref{lemma:reduces}\ref{lemma:reduces:2}, we get the
  reduction $\rho=(\redto{A \land B}{B}{A})$.  Now, we have to
  $\up$-expand $\s_3$.  The leftmost branch generated in
  $\up$-expansion ends with the irreducible sequent $\s_6$.  Actually,
  $\s_3,\dots,\s_6$ define an $\RNCR$-derivation $\Ecal$ of $\s_3$.
  Note that $\rho$ is a reduction for $\s_3$, hence we can apply rule
  $\to E$ of $\RNCR$ to $\rho$ and $\s_3$ to get an $\RNCR$-derivation
  of $\s_2$.  Finally, exploiting the open branch $\s_0,\s_1,\s_2$
  built above, we get the $\RNCR$-derivation $\Dcal$ of $\s_0$.  The
  interpretation extracted from $\Dcal$ is $\Intp{\s_6}=\{p_3,p_4\}$;
  one can check that, for every $\s_i$ in $\Dcal$, $\Intp{\s_6}$
  falsifies $\Form{\s_i}$ (see Lemma~\ref{lemma:RNCRsound}).  We
  stress that there is no need to backtrack in $\s_2$.  If in $\s_2$
  we apply $\ruleRC$, we get the following $\RNCR$-derivation:
  \[\small
  \infer[\to I]{
    \labsequp{}{A \land B \to p_2 \lor \neg p_4}{}{0}}{
    \infer[\lor I]{
      \labsequp{A \land B}{p_2 \lor \neg p_4}{}{1}}{
      \infer[\ruleRC]{
        \labsequp{A \land B}{p_2}{\neg p_4}{2} }{
        \infer[\to I]{
          \sequp{A \land B}{\neg p_4}{p_2}}{
          \infer[\to E]{
            \sequp{p_4,\,A \land B}{\bot}{p_2}}{
            \redto{A \land B}{B}{A} & \infer[\land I_0]{
              \sequp{p_4,\,A}{p_1 \land  p_4}{\bot,\,p_2}}{
              \infer[\ruleIRR]{
                \sequp{p_4,\,A}{p_1}{\bot,\,p_2}}{
              }
            }
          }
        }
      }
    }
  }
  \]
  \EndEs
\end{example}

The example explains the role of  reductions in proof-search:
if we  build an $\NCR$-derivation   of $\s=\seqdown{\G}{H}{A\to B}{\Delta}{\Theta}$
(with $H\not\in\Gamma$)
but we fail to build an $\NCR$-derivation of $\s'=\sequp{\G,\Theta}{A}{\Delta}$,
so that we get an $\RNCR$-derivation of $\s'$, 
from $\s$ we can extract the reduction $\rho=(\redto{\G}{A\to B}{\Theta})$
(see Lemma~\ref{lemma:reduces}\ref{lemma:reduces:2}),
in order to apply the rule $\to\! E$  of $\RNCR$ to $\rho$  and $\s'$.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "clnat"
%%% End:
