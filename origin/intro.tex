\section{Introduction}

The consensus is that natural deduction
calculi~\cite{Prawitz:65,TroSch:00} are not suitable for proof-search
because they lack the ``deep symmetries'' characterizing sequent
calculi (see
e.g.~\cite{DAgostino:05,GirTayLaf:89,Indrzejczakc:2010,SieByr:98,SieCit:2005}
for an accurate discussion). This is evidenced by the fact that this
issue has been scarcely investigated in the literature and the main
contributions~\cite{SieByr:98,SieCit:2005} propose proof-search
strategies which are highly inefficient.  Thus, it seems that the only
effective way to build derivations in natural deduction calculi
consists in translating tableaux or cut-free sequent proofs.  In this
paper we reconsider the problem of proof-search in the natural
deduction calculus for Classical propositional logic ($\CL$) and,
starting from the pioneering ideas in~\cite{SieByr:98,SieCit:2005}, we
define a proof-search procedure to build $\CL$ natural deduction
derivations which has no backtracking points and does not require
loop-checking.


We represent natural deduction derivations in sequent style so that at
each node $\seqnd{\G}{A}$ the open assumptions on which the derivation
of $A$ depends are put in evidence in the \emph{context} $\G$. 
The strategy to build a derivation of $\seqnd{\G}{A}$
presented in~\cite{SieByr:98,SieCit:2005} consists in applying
introduction rules (I-rules) reasoning bottom-up (from the conclusion
to the premises) and elimination rules (E-rules) top-down (from the
premises to the conclusion), in order to ``close the gap'' between
$\Gamma$ and $A$.  Derivations built in this way are \emph{normal}
according to the standard definition~\cite{TroSch:00}.  This approach
can be formalized using the calculus $\NC$ of Fig.~\ref{fig:NC}, which
is the classical counterpart of the intuitionistic natural deduction
calculi of~\cite{DycPin:98,Pfenning:2004}.  Rules of $\NC$ act on two
kinds of judgment, we denote by $\seqncup{\G}{A}$ and
$\seqncdown{\G}{A}$.  A derivation of $\NC$ with root sequent
$\seqncup{\G}{A}$ can be interpreted as a classical normal derivation
of $\seqnd{\G}{A}$ having an I-rule, the classical rule $\botcl$
(reductio ad absurdum) or $\lor\!E$ as root rule
(note that $\bot$ is a primitive constant, $\neg A$ stands for
$A\to \bot$).  A derivation with
root sequent $\seqncdown{\G}{A}$ represents a classical normal
derivation of $\seqnd{\G}{A}$ having the rule $\ruleID$, $\land E_k$
or $\to\! E$ as root rule.  The rule $\coerc$ (coercion), not present in
the usual natural deduction calculus, is a sort of structural rule
which ``coerces'' deductions in normal form.

Using $\NC$, the strategy of~\cite{SieByr:98,SieCit:2005} to search
for a derivation of $\seqnd{\G}{A}$ (that is, a classical normal
derivation of $A$ from assumptions $\Gamma$) can be sketched as
follows.  We start from the sequent $\seqncup{\G}{A}$ and we
$\up$-expand it, by applying bottom-up the I-rules.  In this phase, we
get ``open proof-trees'' (henceforth, we call them \emph{trees})
having $\up$-sequents as leaves.  For each leaf of the kind
$\seqncup{\G}{K}$, we have to find a derivation of 
one of the sequents $\seqncdown{\G}{K}$,
$\seqncdown{\neg K,\G}{\bot}$ and $\seqncdown{\G}{A \lor B}$,
so to match one of the rules $\coerc$, $\botcl$ and $\lor E$.  To search
for a derivation of a $\down$-sequent $\seqncdown{\G}{K}$, 
for every $H\in\Gamma$ we enter a $\down$-phase:
we $\down$-expand the axiom
sequent $\seqncdown{\G}{H}$ by applying downwards the rules $\land
E_k$ and $\to E$ until we get a tree with root $\seqncdown{\G}{K}$.
In general, these two expansion steps must be interleaved.  For
instance, when in $\down$-expansion we apply the rule $\to\!E$ to a
tree with root $\seqncdown{\G}{A\to B}$, we get $\seqncdown{\G}{B}$ as
new root and $\seqncup{\G}{A}$ as new leaf.  Thus, to turn the tree
into a derivation, we must enter a new $\up$-expansion phase to get a
derivation of $\seqncup{\G}{A}$.  This na\"ive strategy suffers from
the huge search space.  This is due to many factors: firstly, contexts
cannot decrease, thus an assumption might be used more and more
times. As a consequence termination is problematic;
in~\cite{SieByr:98,SieCit:2005}, it is guaranteed by loop-checking.
Secondly, the na\"ive strategy has potentially many backtrack points.
This is in disagreement with the proof-search strategies based on
standard sequent/tableaux calculi for $\CL$, where a formula
occurrence can be used at most once along a branch, no backtracking is
needed and termination is guaranteed by the fact that at each step at
least a formula is decomposed.  The main objective of this paper is to
show that we can recover these nice properties even in natural
deduction calculi, provided we add more structure to the sequents.


To achieve our goal, we exploit some ideas introduced
in~\cite{GabOli:2000} and some techniques introduced
in~\cite{FerFioFio:2013tab,FerFioFio:2015tocl} in the context of
proof-search for the intuitionistic sequent calculus
$\GJ$~\cite{TroSch:00}.  One of the main issue in the calculus $\NC$
is that rule $\botcl$, read bottom-up, introduces a negative formula
$\neg A$ in the context.  This fact breaks the (strict) subformula
property for $\NC$.  Moreover, since contexts never decrease, the
assumption $\neg A$ can be used many times during proof-search, and
this might generate branches containing infinitely many sequents of
the kind $\seqncup{\G}{A}$.  To avoid this, we replace the classical
rule $\botcl$ with the intuitionistic version $\botint$ and we
introduce the \emph{restart} rule of~\cite{GabOli:2000} to recover the
classical reductio ad absurdum reasoning.  Basically, we simulate rule
$\botcl$ by storing some of the formulas obtained in the right-hand
side of $\up$-sequents in a set $\Delta$, we call \emph{restart set};
such formulas can be resumed by applying one of the restart rules
$\ruleRC$ and $\ruleRP$ (see Fig.~\ref{fig:NCR}).  To implement this,
an $\up$-sequent has now the form $\sequp{\G}{A}{\Delta}$, where $\G$
is the set of assumptions, $A$ the formula to be proved and $\Delta$
the restart set.  The other key point is the management of resources,
namely of available assumptions.  The idea is to follow the
\emph{LL(Local Linear)-computation} paradigm of~\cite{GabOli:2000}, in
order to control the use of assumptions and avoid redundancies in
derivations.  In~\cite{GabOli:2000} restart and LL-computation are
combined to provide a goal-oriented proof-search strategy for the
$\to$-fragment of $\CL$.  The extension to the full language is not
immediate; actually,~\cite{GabOli:2000} treats the full language via
reduction of formulas to disjunctive normal form.  One of the main
problems arises with the application of rules for $\land$ elimination.
Indeed, let us assume to apply $\land E_0$ to $\down$-expand a
derivation $\Dcal$ with root sequent $\seqncdown{\G}{A\land B}$ so to
get the sequent $\seqncdown{\G}{A}$. The resource $B$ discarded by
$\land E_0$ is lost and, to recover it, we should rebuild $\Dcal$ and
apply $\land E_1$.  To avoid this overload, we endow the
$\down$-sequents with a supplementary \emph{resource set} $\Theta$,
where we store the formulas discarded by $\land E_k$ applications, so
that they are available as assumptions in successive $\up$-expansion
phases.  A $\down$-sequent has now the form
$\seqdown{\G}{H}{A}{\D}{\Theta}$, where $\G$, $A$ and $\Delta$ have
the same meaning seen above, the displayed assumption $H$ is the
\emph{head formula} (as defined in~\cite{TroSch:00}) and $\Theta$ is
the resource set.  Formulas in $\Theta$ are regained as assumptions in
the $\up$-premises of rules $\lor E$ and $\to E$.  This leads to the
calculus $\NCR$ ($\NC$ with restart) of Fig.~\ref{fig:NCR}.




Calculi $\NC$ and $\NCR$ are presented in Sect.~\ref{sec:NCR}; here we show
that $\NCR$-derivations have a direct translations into $\NC$, so that
$\NCR$ can be viewed as a notational variant of $\NC$.  In
Sect.~\ref{sec:search} we formally describe the proof-search procedure
outlined above and we prove its correctness: if a sequent $\s$ is
valid, the procedure yields an $\NCR$-derivation of $\s$.  To prove
this, in Sect.~\ref{sec:ref} we introduce the calculus $\RNCR$ for
classical unprovability, which is the dual of $\NCR$. We show that, if
$\s$ is provable in $\RNCR$, then $\s$ is not valid; actually, from an
$\RNCR$-derivation of $\s$ we can extract a classical interpretation
witnessing the non-validity of $\s$.




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "clnat"
%%% End:
