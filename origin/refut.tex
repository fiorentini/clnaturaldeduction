

\section{The refutation calculus $\RNCR$}\label{sec:ref}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  REFUTATION CALCULUS RNCR 

\begin{figure}[t]\small
  \[
  \begin{array}{c}
    %% Ref NCR Irr
    \AXC{}
    \RightLabel{$\ruleIRR$}
\UIC{$\s$}
   % \UIC{$\sequp{\G}{F}{\Delta}{}$}
    \DP
\quad\mbox{$\s$ irreducible}
  \hspace{3em}
%%% Ref NCR Restart COMP
     \AXC{$\sequp{\G}{D}{F,\D_D}$}
     \RightLabel{$\ruleRC$}
     \UIC{$\sequp{\G}{F}{D,\D}$}
 \DP
\quad D\not\in\Prime,\, F\in\Prime
\\[4ex]
%%% RNCR AND I
     \AXC{$\sequp{\G}{A_k}{\D}$}
     \RightLabel{$\land I_k$}
     \UIC{$\sequp{\G}{A_0\land A_1}{\D}$}
 \DP
%\quad k\in\{0,1\} 
 \hspace{2em}
%%% RNCR OR I
     \AXC{$\sequp{\G}{A}{B,\D}$}
     \RightLabel{$\lor I$}
     \UIC{$\sequp{\G}{A\lor B}{\D}$}
 \DP
\hspace{2em}
%%% RNCR IMPL I
     \AXC{$\sequp{A,\G}{B}{\D}$}
     \RightLabel{$\to I$}
     \UIC{$\sequp{\G}{A \to B}{\D}$}
 \DP
\\[4ex]
\begin{array}{cc}
%%% RNCR OR E_k
\AXC{$\redto{H}{A_0\lor A_1}{\Theta}$}
\AXC{$\sequp{A_k,\G_H,\Theta}{F}{\D}$}
\RightLabel{$\lor E_k$}
\BIC{$\sequp{H,\G}{F}{\D}$}
 \DP
&\quad F\in\Prime,\,k\in\{0,1\}
 \\[4ex]
% %%% RNCR  IMPL  E
 \AXC{$\redto{H}{A\to  B}{\Theta}$}
 \AXC{$\sequp{\G_H,\Theta}{A}{F,\D}$}
 \RightLabel{$\to E$}
 \BIC{$\sequp{H,\G}{F}{\D}$}
  \DP
  &\quad  F\in\Prime
\end{array}
\end{array}
   \]
  \caption{The  refutation calculus $\RNCR$.}
  \label{fig:RNCR}
\end{figure}        

To prove the completeness of $\NCR$ and the correctness of the
proof-search strategy defined in Sect.~\ref{sec:search}, we introduce
the calculus $\RNCR$ for classical unprovability. This calculus is
dual to $\NCR$ in the following sense: from a failed proof-search of
an $\NCR$-derivation of $\s$ we can extract an $\RNCR$-derivation of
$\sigma$ (see Sect.~\ref{sec:search}).  Since sequents provable in
$\RNCR$ are not valid (see Prop.~\ref{prop:soundRNCR} below), we get
the completeness of $\NCR$; indeed, if $\s$ is valid, the proof-search
procedure must return an $\NCR$-derivation of $\sigma$.

Let the \emph{reduction relation} $\redtoname$ be the smallest
relation defined by the rules:
\[
%%% REDUCTION ID
\AXC{}
\UIC{$\redto{H}{H}{}$}
\DP
\qquad
%%% REDUCTION AND
\AXC{$\redto{H}{A_0\land A_1}{\Theta}$}
\UIC{$\redto{H}{A_k}{A_{1-k},\Theta}$}
\DP
\;k\in\{0,1\}
\qquad
%%% REDUCTION IMPL
\AXC{$\redto{H}{A \to B}{\Theta}$}
\UIC{$\redto{H}{B}{\Theta}$}
\DP
\]
where $A$, $B$, $H$ are formulas, and $\Theta$ is a (possibly empty)
finite set of formulas. The following properties of $\redtoname$ can
be easily proved:

\begin{lemma}\label{lemma:reduces}
 \mbox{}
  \begin{enumerate}[label=(\roman*),ref=(\roman*)]
  \item\label{lemma:reduces:1} If $\redto{H}{K}{\Theta}$, then
    $K\in\posSf{H}$, $\Theta\subseteq\posSfm{H}$ and $K\land
    (\bigwedge\Theta) \to H$ is valid.

  \item\label{lemma:reduces:2}
    $\NCR\der\seqdown{\G}{H}{K}{\D}{\Theta}$ implies
    $\redto{H}{K}{\Theta}$.
  \end{enumerate}
\end{lemma}

\begin{proof}
   Point~\ref{lemma:reduces:1} follows by induction on the depth of the
   derivation of $\redto{H}{K}{\Theta}$.  Point~\ref{lemma:reduces:2}
   can be proved by induction on the depth of the $\NCR$-derivation of
   $\seqdown{\G}{H}{K}{\D}{\Theta}$.\qed
\end{proof}

\noindent
An $\up$-sequent $\sigirr=\sequp{\Gamma}{F}{\Delta}$ is
\emph{irreducible} iff the following conditions hold:

\begin{enumerate}[label=($\mathrm{Irr}\arabic*$),ref=($\mathrm{Irr}\arabic*$)]
\item\label{irr1} $(\{F\}\cup\Delta )\;\subseteq\; \Prime$;
 
\item\label{irr2} $(\{\bot,F\}\cup\Delta)\,\cap\, \posSf{\Gamma} =\emptyset$
  and  $A\lor B\not\in\posSf{\Gamma}$, for every $A\lor B\in\Lcal$.
\end{enumerate}


\noindent
In proof-search, when in an $\up$-expansion phase we get an
irreducible sequent $\sigirr$, the phase ends. As a matter of fact, by
condition~\ref{irr1} no rule of $\rulesup$ can be applied to
$\sigirr$; by condition~\ref{irr2}, $\sigirr$ does not admit any
closing match.  Irreducible sequents are not valid. Indeed, let
$\Intp{\sigirr} =\posSf{\Gamma}\cap\PV$ be the \emph{interpretation}
associated with $\sigirr$.  We can prove that:

\begin{lemma}\label{lemma:irr}
  Let $\sigirr$ be an irreducible sequent.  Then,
  $\Intp{\sigirr}\not\models \Fm{\sigirr}$.
\end{lemma}

\begin{proof}
  Let $\sigirr=\sequp{\Gamma}{F}{\Delta}$.  By induction on $A$
  and~\ref{irr2}, we can show that $A\in\posSf{\Gamma}$ implies
  $\Intp{\sigirr}\models A$; thus $\Intp{\sigirr}\models\bigwedge\G$.
  Moreover, by~\ref{irr1} and~\ref{irr2} we get
  $\Intp{\sigirr}\not\models F\lor(\bigvee\D)$. Hence
  $\Intp{\sigirr}\not\models \Fm{\sigirr}$.  \qed
\end{proof}

Rules of the calculus $\RNCR$ are shown in Fig.~\ref{fig:RNCR}.  Rules
$\lor E_k$ and $\to\! E$ have, as premises, a reduction $\rho$ (side
condition) and an $\up$-sequent $\s$; we say that $\rho$ is a
\emph{reduction for} $\s$.

An $\RNCR$-derivation $\Dcal$ consists of a single branch whose
top-most sequent is irreducible; we call it the
\emph{irreducible sequent of $\Dcal$}.  

\begin{lemma}\label{lemma:RNCRsound}
  Let $\Dcal$ be an $\RNCR$-derivation and let $\sigirr$ be the
  irreducible sequent of $\Dcal$.  For every $\s$ occurring in
  $\Dcal$, $\Intp{\sigirr}\not\models\Fm{\s}$.
\end{lemma}

\begin{proof}
  By induction on the depth $d$ of $\s$.  If $d=0$, then $\s=\sigirr$
  and the assertion follows by Lemma~\ref{lemma:irr}.  Let $\s$ be the
  conclusion of an application of one of the rules $\lor E_k$, $\to\!
  E$ with side condition $\rho$ and premise $\s'$.  By induction
  hypothesis $\Intp{\sigirr}\not\models\Fm{\s'}$; by
  Lemma~\ref{lemma:reduces}\ref{lemma:reduces:1}, we get
  $\Intp{\sigirr}\not\models\Fm{\s}$.  The other cases easily follow.
  \qed
\end{proof}

Accordingly, from an $\RNCR$-derivation of $\s$ we can extract an
interpretation falsifying $\s$, namely the interpretation  $\Intp{\sigirr}$.
This implies that:


\begin{proposition}[Soundness of $\RNCR$]\label{prop:soundRNCR}
  If $\RNCR\der \s$ then $\s$ is not valid.
\end{proposition}


\begin{example}
  Let us consider the following $\NCR$-tree $\Dcal$ built according
  with the above discussed proof-search procedure.
  \[\small
  \begin{array}{c}
    A \,=\, (p_1 \to p_2) \to p_3
    \qquad
    B\,=\, p_1 \land  p_4 \to p_2
    \\[1.5ex]
    \infer[\to\textrm{I}]{
      \sequp{}{A \land B \to p_2 \lor  \neg p_4  }{}_0}{
      \infer[\lor\textrm{I}]{
        \sequp{A \land B }{p_2 \lor  \neg p_4 }{}_1}{
        \infer[\down\Uparrow]{
          \sequp{A \land B }{p_2}{ \neg p_4}_2}{
          \infer[\to\textrm{E}]{
            \seqdown{}{A \land B }{p_2}{p_2,  \neg p_4}{A }_c}{
            \infer[\land\textrm{E}_1]{
              \seqdown{}{A \land B }{B}{p_2,  \neg p_4}{A }_b}{
              \infer[\mathrm{Id}]{
                \seqdown{}{A \land B }{A \land B }{p_2,  \neg p_4}{}_a}{
              }
            }
            &
            \infer[\land\textrm{I}]{
              \sequp{A }{p_1 \land p_4 }{p_2,  \neg p_4}_3}{
              \infer[\textrm{R}_\textrm{c}]{
                \sequp{A }{p_1}{p_2,  \neg p_4}_4}{
                \infer[\to\textrm{I}]{
                  \sequp{A }{ \neg p_4}{p_1, p_2}_5}{
                  \sequp{p_4,A}{\bot}{p_1, p_2}_6}
              }&\dots
            }
          }
        }
      }
    }
  \end{array}
  \]
  The $\up$-expansion of $\s_0$ yields $\s_2$.  Here we can
  non-deterministically choose either to continue the $\up$-expansion
  phase by applying $\ruleRC$ (restart from $\neg p_4$) or to start a
  $\down$-expansion phase from $\sigma_a =\seqdown{}{A\land B}{A\land
    B}{p_2,\neg p_4}{}$ since $\stru{A\land B, p_2,\coerc}$ is a
  closing match for $\s_2$.  We follow the latter way.
  By~\ref{inv:down}, we apply $\land E_1$ to $\sigma_a$ and we get
  $\sigma_b$.  By applying $\to E$, we get the conclusion $\sigma_c$
  and the premise $\s_3$, which must be $\up$-expanded.  At this point
  the $\down$-expansion stops since, applying $\coerc$ with premise
  $\sigma_c$, we conclude $\s_2$.  We have to $\up$-expand $\s_3$. The
  leftmost branch (shown above) ends with the irreducible sequent
  $\s_6$ and the proof-search fails since no closing match exists for
  $\s_6$. From $\Dcal$ we can extract the information needed to build
  the $\RNCR$-derivation $\Ecal$ of $\s_0$ displayed below.  Note that
  paths $\s_0,\dots,\s_2$ and $\s_3,\dots,\s_6$ of $\Dcal$ immediately
  correspond to paths in $\Ecal$. Moreover, from the $\NCR$-derivation
  of $\s_b$ contained in $\Dcal$, by
  Lemma~\ref{lemma:reduces}\ref{lemma:reduces:2}, we get the reduction
  $(\redto{A \land B}{B}{A})$.  The interpretation extracted from
  $\Ecal$ is $\Intp{\s_6}=\posSf{p_4,A}\cap\PV=\{p_3,p_4\}$; one can
  check that, according with Lemma~\ref{lemma:RNCRsound}, 
  \begin{minipage}{1.0\linewidth}
    \begin{tabular}{p{.62\linewidth}p{.38\linewidth}}
      \begin{minipage}{\linewidth}
  \[\small
  \begin{array}{c}
    \infer[\to I]{
      \labsequp{}{A \land B\to p_2\lor \neg p_4}{}{0}}{
      \infer[\lor I]{
        \labsequp{A\land B}{p_2\lor \neg p_4}{}{1}}{
        \infer[\to E]{
          \labsequp{A \land B}{p_2}{\neg p_4}{2}}{
          \redto{A \land B}{B}{A} & \infer[\land I_0]{
            \labsequp{A}{p_1 \land  p_4}{p_2,\,\neg p_4}{3}}{
            \infer[\ruleRC]{
              \labsequp{A}{p_1}{p_2,\neg p_4}{4}}{
              \infer[\to I]{
                \labsequp{A}{\neg p_4}{p_1,p_2}{5}}{
                \infer[\ruleIRR]{
                  \labsequp{p_4,\,A}{\bot}{p_1,p_2}{6}}
                {}
              }
            }
          }
        }
      }
    }
  \end{array}
  \]
  \end{minipage}
  &
  \begin{minipage}{\linewidth}
    for every $\s_i$ in $\Dcal$, $\Intp{\s_6}$ falsifies
    $\Form{\s_i}$.  We stress that searching for an $\NCR$-derivation
    of $\s_0$ there is no need to backtrack in $\s_2$.  Applying
    $\ruleRC$ to $\s_2$ instead of $\coerc$, we fail to build an
    $\NCR$-derivation of $\s_0$ and we get an $\RNCR$-derivation of
    $\s_0$ as well.
    \EndEs
  \end{minipage}
\end{tabular}
\end{minipage}\\[.5ex]

\end{example}


% \begin{example}
%   Let us consider the following $\RNCR$-derivation $\Dcal$: 
%   %where
%   %$A=(p_1 \to p_2) \to p_3$ and $B= p_1 \land p_4 \to p_2$:
%   \[\small
%    \begin{array}{c}
%     A \,=\, (p_1 \to p_2) \to p_3
%      \qquad
%      B\,=\, p_1 \land  p_4 \to p_2
%      \\[.5ex]
%     \infer[\to I]{
%       \labsequp{}{A \land B\to p_2\lor \neg p_4}{}{0}}{
%       \infer[\lor I]{
%         \labsequp{A\land B}{p_2\lor \neg p_4}{}{1}}{
%         \infer[\to E]{
%           \labsequp{A \land B}{p_2}{\neg p_4}{2}}{
%           \redto{A \land B}{B}{A} & \infer[\land I_0]{
%             \labsequp{A}{p_1 \land  p_4}{p_2,\,\neg p_4}{3}}{
%             \infer[\ruleRC]{
%               \labsequp{A}{p_1}{p_2,\neg p_4}{4}}{
%               \infer[\to I]{
%                 \labsequp{A}{\neg p_4}{p_1,p_2}{5}}{
%                 \infer[\ruleIRR]{
%                   \labsequp{p_4,\,A}{\bot}{p_1,p_2}{6}}
%                 {}
%               }
%             }
%           }
%         }
%       }
%     }
%   \end{array}
%   \]
%   The construction of $\Dcal$ follows by the failure of the
%   proof-search for $\s_0$.  Indeed, $\up$-expansion of $\s_0$ yields
%   $\s_2$.  Here we can non-deterministically choose either to continue
%   the $\up$-expansion phase by applying $\ruleRC$ (restart from $\neg
%   p_4$) or to start a $\down$-expansion phase from $\sigdown_0
%   =\seqdown{}{A\land B}{A\land B}{\D}{}$ with $\D=\{p_2,\neg p_4 \}$
%   (indeed, $\stru{A\land B, p_2,\coerc}$ is a closing match for
%   $\s_2$).  We follow the latter way.  By~\ref{inv:down}, we apply
%   $\land E_1$ and we get $\sigdown_1 =\seqdown{}{A\land B}{B}{\D}{A}$.
%   By applying $\to E$, we get the conclusion $\sigdown_2
%   =\seqdown{}{A\land B}{p_2}{\D}{A}$ and the premise $\s_3$, which
%   must be $\up$-expanded.  The $\down$-expansion phase ends, since we
%   can apply $\coerc$ with premise $\sigdown_2$ and conclusion $\s_2$.
%   Note that we have built an $\NCR$-derivation of $\sigdown_1$ hence,
%   by Lemma~\ref{lemma:reduces}\ref{lemma:reduces:2}, we get the
%   reduction $\rho=(\redto{A \land B}{B}{A})$.  Now, we have to
%   $\up$-expand $\s_3$.  The leftmost branch generated in
%   $\up$-expansion ends with the irreducible sequent $\s_6$.  Actually,
%   $\s_3,\dots,\s_6$ define an $\RNCR$-derivation $\Ecal$ of $\s_3$.
%   Note that $\rho$ is a reduction for $\s_3$, hence we can apply rule
%   $\to E$ of $\RNCR$ to $\rho$ and $\s_3$ to get an $\RNCR$-derivation
%   of $\s_2$.  Finally, exploiting the open branch $\s_0,\s_1,\s_2$
%   built above, we get the $\RNCR$-derivation $\Dcal$ of $\s_0$.  The
%   interpretation extracted from $\Dcal$ is $\Intp{\s_6}=\{p_3,p_4\}$;
%   one can check that, for every $\s_i$ in $\Dcal$, $\Intp{\s_6}$
%   falsifies $\Form{\s_i}$ (see Lemma~\ref{lemma:RNCRsound}).  We
%   stress that there is no need to backtrack in $\s_2$.  If in $\s_2$
%   we apply $\ruleRC$, we get the following $\RNCR$-derivation:
%   \[\small
%   \infer[\to I]{
%     \labsequp{}{A \land B \to p_2 \lor \neg p_4}{}{0}}{
%     \infer[\lor I]{
%       \labsequp{A \land B}{p_2 \lor \neg p_4}{}{1}}{
%       \infer[\ruleRC]{
%         \labsequp{A \land B}{p_2}{\neg p_4}{2} }{
%         \infer[\to I]{
%           \sequp{A \land B}{\neg p_4}{p_2}}{
%           \infer[\to E]{
%             \sequp{p_4,\,A \land B}{\bot}{p_2}}{
%             \redto{A \land B}{B}{A} & \infer[\land I_0]{
%               \sequp{p_4,\,A}{p_1 \land  p_4}{\bot,\,p_2}}{
%               \infer[\ruleIRR]{
%                 \sequp{p_4,\,A}{p_1}{\bot,\,p_2}}{
%               }
%             }
%           }
%         }
%       }
%     }
%   }
%   \]
%   \EndEs
% \end{example}

\noindent
The above example explains the role of  reductions in proof-search:
if we  build an $\NCR$-derivation   of $\s=\seqdown{\G}{H}{A\to B}{\Delta}{\Theta}$
(with $H\not\in\Gamma$)
but we fail to build an $\NCR$-derivation of $\s'=\sequp{\G,\Theta}{A}{\Delta}$,
so that we get an $\RNCR$-derivation of $\s'$, 
from $\s$ we can extract the reduction $\rho=(\redto{\G}{A\to B}{\Theta})$
(see Lemma~\ref{lemma:reduces}\ref{lemma:reduces:2}),
in order to apply the rule $\to\! E$  of $\RNCR$ to $\rho$  and $\s'$.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "clnat"
%%% End:
